<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

add_action( 'init', 'pdf_forms1' );
require_once('tcpdf_include.php');
$plugin_dir = ABSPATH . 'wp-content/plugins/pdf_form/includes/tcpdf.php';
///$dir1 = $dir.'/pdf_form/includes/tcpdf.php';
//echo $dir1;
require_once($plugin_dir);
//require_once('/../tcpdf.php');
function pdf_forms1() {
	
	add_shortcode( 'show_main_form', 'main_form11' );
}
function main_form11() {
// create new PDF document
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 002');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', 'BI', 20);

// add a page
$pdf->AddPage();

// set some text to print
$name = 'asdasdada';

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// Set some content to print
$html = 'In the Matter of the Marriage of'.$user_name.'and Case No. 		
'.$spouse_name.' date of merriege is '.$date_of_marraige;

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
$dir = plugin_dir_path( __FILE__ );
$filename = 'file22.pdf';

// Create the full path
$full_path = $dir . '/' . $filename;
print_r($full_path);
// Output PDF
$pdf->Output($full_path, 'F');

//============================================================+
// END OF FILE
//============================================================+
}
ob_start();
?>
<form method="post" enctype="multipart/form-data" id="gform_18" action="/dev/5jointpetitiondivorcewchildren/">
   <div class="gform_body">
      <ul id="gform_fields_18" class="gform_fields top_label form_sublabel_below description_below">
         <li id="field_18_1" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_1">CODE:</label>
            <div class="ginput_container ginput_container_text"><input name="input_1" id="input_18_1" type="text" value="" class="medium" tabindex="1" aria-invalid="false"></div>
         </li>
         <li id="field_18_2" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_2">Your Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_2" id="input_18_2" type="text" value="" class="medium" tabindex="2" aria-invalid="false"></div>
         </li>
         <li id="field_18_3" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_3">Spouse’s Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_3" id="input_18_3" type="text" value="" class="medium" tabindex="3" aria-invalid="false"></div>
         </li>
         <li id="field_18_4" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_4">Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_4" id="input_18_4" type="text" value="" class="medium" tabindex="4" aria-invalid="false"></div>
         </li>
         <li id="field_18_5" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_5">Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_5" id="input_18_5" type="text" value="" class="medium" tabindex="5" aria-invalid="false"></div>
         </li>
         <li id="field_18_6" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_6">City, State, Zip</label>
            <div class="ginput_container ginput_container_text"><input name="input_6" id="input_18_6" type="text" value="" class="medium" tabindex="6" aria-invalid="false"></div>
         </li>
         <li id="field_18_7" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_7">City, State, Zip</label>
            <div class="ginput_container ginput_container_text"><input name="input_7" id="input_18_7" type="text" value="" class="medium" tabindex="7" aria-invalid="false"></div>
         </li>
         <li id="field_18_8" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_8">Phone</label>
            <div class="ginput_container ginput_container_phone"><input name="input_8" id="input_18_8" type="text" value="" class="medium" tabindex="8" aria-invalid="false"></div>
         </li>
         <li id="field_18_9" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_9">Phone</label>
            <div class="ginput_container ginput_container_phone"><input name="input_9" id="input_18_9" type="text" value="" class="medium" tabindex="9" aria-invalid="false"></div>
         </li>
         <li id="field_18_10" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h2 style="text-align: center;"><strong>IN THE FAMILY DIVISION</strong></h2>
            <h2 style="text-align: center;"><strong>OF THE SECOND JUDICIAL DISTRICT COURT OF THE STATE OF NEVADA</strong></h2>
            <h2 style="text-align: center;"><strong>IN AND FOR THE COUNTY OF WASHOE</strong></h2>
         </li>
         <li id="field_18_11" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label gfield_label_before_complex" for="input_18_11_3">In the Matter of the Marriage of</label>
            <div class="ginput_complex ginput_container no_prefix has_first_name no_middle_name has_last_name no_suffix gf_name_has_2 ginput_container_name gfield_trigger_change" id="input_18_11">
               <span id="input_18_11_3_container" class="name_first">
               <input type="text" name="input_11.3" id="input_18_11_3" value="" aria-label="First name" tabindex="11" aria-invalid="false">
               <label for="input_18_11_3">YOUR NAME</label>
               </span>
               <span id="input_18_11_6_container" class="name_last">
               <input type="text" name="input_11.6" id="input_18_11_6" value="" aria-label="Last name" tabindex="13" aria-invalid="false">
               <label for="input_18_11_6">SPOUSE’S NAME</label>
               </span>
            </div>
         </li>
         <li id="field_18_12" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_12">Case No.</label>
            <div class="ginput_container ginput_container_text"><input name="input_12" id="input_18_12" type="text" value="" class="medium" tabindex="15" aria-invalid="false"></div>
         </li>
         <li id="field_18_13" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_13">Dept No:</label>
            <div class="ginput_container ginput_container_text"><input name="input_13" id="input_18_13" type="text" value="" class="medium" tabindex="16" aria-invalid="false"></div>
         </li>
         <li id="field_18_14" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_14">Petitioners.</label>
            <div class="ginput_container ginput_container_text"><input name="input_14" id="input_18_14" type="text" value="" class="medium" tabindex="17" aria-invalid="false"></div>
         </li>
         <li id="field_18_15" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h2 style="text-align: center;"><strong><u>JOINT PETITION FOR SUMMARY DECREE OF DIVORCE </u></strong></h2>
            <h2 style="text-align: center;"><strong><u>WITH MINOR CHILD(REN)</u></strong></h2>
         </li>
         <li id="field_18_16" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">COMES NOW, Petitioner</p>
         </li>
         <li id="field_18_17" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_17"></label>
            <div class="ginput_container ginput_container_text"><input name="input_17" id="input_18_17" type="text" value="" class="medium" tabindex="18" aria-invalid="false"></div>
            <div class="gfield_description">(YOUR NAME)</div>
         </li>
         <li id="field_18_18" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">in proper person, and </p>
         </li>
         <li id="field_18_19" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_19"></label>
            <div class="ginput_container ginput_container_text"><input name="input_19" id="input_18_19" type="text" value="" class="medium" tabindex="19" aria-invalid="false"></div>
            <div class="gfield_description">(SPOUSE’S NAME )</div>
         </li>
         <li id="field_18_20" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br>
            <p style="text-align: left;">in proper person, hereby petition this Court, pursuant to the terms of Chapter 125 of the Nevada Revised Statutes, to grant them a divorce. Petitioners respectfully show, and under oath, state to the Court that every condition of NRS 125.181 has been met and further state as follows:</p>
         </li>
         <li id="field_18_21" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h2 style="text-align: center;"><strong>I.</strong></h2>
         </li>
         <li id="field_18_22" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">The Petitioners were married on </p>
         </li>
         <li id="field_18_23" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_23"></label>
            <div class="ginput_container ginput_container_text"><input name="input_23" id="input_18_23" type="text" value="" class="medium" tabindex="20" aria-invalid="false"></div>
            <div class="gfield_description">DATE OF MARRIAGE</div>
         </li>
         <li id="field_18_24" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">, in the city of </p>
         </li>
         <li id="field_18_25" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_25"></label>
            <div class="ginput_container ginput_container_text"><input name="input_25" id="input_18_25" type="text" value="" class="medium" tabindex="21" aria-invalid="false"></div>
            <div class="gfield_description">CITY, COUNTY </div>
         </li>
         <li id="field_18_26" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;"> County, State of </p>
         </li>
         <li id="field_18_27" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_27"></label>
            <div class="ginput_container ginput_container_text"><input name="input_27" id="input_18_27" type="text" value="" class="medium" tabindex="22" aria-invalid="false"></div>
            <div class="gfield_description">STATE</div>
         </li>
         <li id="field_18_28" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;"> and ever since that day have been, and are now, husband and wife.  </p>
         </li>
         <li id="field_18_29" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h2 style="text-align: center;"><strong>II.</strong></h2>
         </li>
         <li id="field_18_30" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">Petitioner</p>
         </li>
         <li id="field_18_31" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_31"></label>
            <div class="ginput_container ginput_container_text"><input name="input_31" id="input_18_31" type="text" value="" class="medium" tabindex="23" aria-invalid="false"></div>
            <div class="gfield_description">YOUR NAME or Resident Petitioner </div>
         </li>
         <li id="field_18_32" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">is a resident of the State of Nevada, County of </p>
         </li>
         <li id="field_18_33" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_33"></label>
            <div class="ginput_container ginput_container_text"><input name="input_33" id="input_18_33" type="text" value="" class="medium" tabindex="24" aria-invalid="false"></div>
            <div class="gfield_description">COUNTY</div>
         </li>
         <li id="field_18_34" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br>
            <p style="text-align: left;">, and for a period of more than six (6) weeks immediately preceding the commencement of this action, has resided in, been physically present in, and is a resident of the State of Nevada, and intends to continue to make the State of Nevada his/her home for an indefinite period of time  </p>
         </li>
         <li id="field_18_35" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: left;"><strong>The current addresses of the Petitioners are:</strong></h3>
         </li>
         <li id="field_18_36" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_36">Your Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_36" id="input_18_36" type="text" value="" class="medium" tabindex="25" aria-invalid="false"></div>
         </li>
         <li id="field_18_37" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_37">Spouse’s Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_37" id="input_18_37" type="text" value="" class="medium" tabindex="26" aria-invalid="false"></div>
         </li>
         <li id="field_18_38" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_38">Address</label>
            <div class="ginput_container ginput_container_textarea"><textarea name="input_38" id="input_18_38" class="textarea medium" tabindex="27" aria-invalid="false" rows="10" cols="50"></textarea></div>
         </li>
         <li id="field_18_39" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_39">Address</label>
            <div class="ginput_container ginput_container_textarea"><textarea name="input_39" id="input_18_39" class="textarea medium" tabindex="28" aria-invalid="false" rows="10" cols="50"></textarea></div>
         </li>
         <li id="field_18_40" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_40">Mailing Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_40" id="input_18_40" type="text" value="" class="medium" tabindex="29" aria-invalid="false"></div>
         </li>
         <li id="field_18_41" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_41">Mailing Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_41" id="input_18_41" type="text" value="" class="medium" tabindex="30" aria-invalid="false"></div>
         </li>
         <li id="field_18_42" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: center;"><strong>III.</strong></p>
            <p style="text-align: left;">The Petitioners have become, and continue to be, incompatible in marriage and no reconciliation is possible.</p>
         </li>
         <li id="field_18_43" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: center;"><strong>IV.</strong></p>
            <p style="text-align: left;">Wife is not pregnant at this time<br>
               If wife is pregnant , then fill in below, if not, remove:<br>
               Husband is/is not the father of the unborn child. The unborn child is due to be born on:
            </p>
         </li>
         <li id="field_18_44" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: center;"><strong>V.</strong></h3>
         </li>
         <li id="field_18_45" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">There are</p>
         </li>
         <li id="field_18_46" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_46"></label>
            <div class="ginput_container ginput_container_text"><input name="input_46" id="input_18_46" type="text" value="" class="medium" tabindex="31" aria-invalid="false"></div>
            <div class="gfield_description">Number</div>
         </li>
         <li id="field_18_47" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br>
            <p style="text-align: left;">Tminor children born to, or adopted, through this union</p>
         </li>
         <li id="field_18_62" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible"></li>
         <li id="field_18_48" class="gfield gf_left_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_48">Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_48" id="input_18_48" type="text" value="" class="medium" tabindex="32" aria-invalid="false"></div>
         </li>
         <li id="field_18_49" class="gfield gf_middle_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_49">Age</label>
            <div class="ginput_container ginput_container_text"><input name="input_49" id="input_18_49" type="text" value="" class="medium" tabindex="33" aria-invalid="false"></div>
         </li>
         <li id="field_18_50" class="gfield gf_right_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_50">DATE OF BIRTH</label>
            <div class="ginput_container ginput_container_text"><input name="input_50" id="input_18_50" type="text" value="" class="medium" tabindex="34" aria-invalid="false"></div>
         </li>
         <li id="field_18_51" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: center;"><strong>VI.</strong></h3>
         </li>
         <li id="field_18_52" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: left;"><strong>The State of residence of the children is as follows:</strong></p>
         </li>
         <li id="field_18_53" class="gfield gf_left_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_53">Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_53" id="input_18_53" type="text" value="" class="medium" tabindex="35" aria-invalid="false"></div>
         </li>
         <li id="field_18_54" class="gfield gf_middle_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_54">State of Residence</label>
            <div class="ginput_container ginput_container_text"><input name="input_54" id="input_18_54" type="text" value="" class="medium" tabindex="36" aria-invalid="false"></div>
         </li>
         <li id="field_18_55" class="gfield gf_right_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_55">Length of Time Child Has Lived in that State</label>
            <div class="ginput_container ginput_container_text"><input name="input_55" id="input_18_55" type="text" value="" class="medium" tabindex="37" aria-invalid="false"></div>
         </li>
         <li id="field_18_56" class="gfield gf_left_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_56">Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_56" id="input_18_56" type="text" value="" class="medium" tabindex="38" aria-invalid="false"></div>
         </li>
         <li id="field_18_57" class="gfield gf_middle_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_57">State of Residence</label>
            <div class="ginput_container ginput_container_text"><input name="input_57" id="input_18_57" type="text" value="" class="medium" tabindex="39" aria-invalid="false"></div>
         </li>
         <li id="field_18_58" class="gfield gf_right_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_58">Length of Time Child Has Lived in that State</label>
            <div class="ginput_container ginput_container_text"><input name="input_58" id="input_18_58" type="text" value="" class="medium" tabindex="40" aria-invalid="false"></div>
         </li>
         <li id="field_18_59" class="gfield gf_left_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_59">Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_59" id="input_18_59" type="text" value="" class="medium" tabindex="41" aria-invalid="false"></div>
         </li>
         <li id="field_18_60" class="gfield gf_middle_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_60">State of Residence</label>
            <div class="ginput_container ginput_container_text"><input name="input_60" id="input_18_60" type="text" value="" class="medium" tabindex="42" aria-invalid="false"></div>
         </li>
         <li id="field_18_61" class="gfield gf_right_third field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_61">Length of Time Child Has Lived in that State</label>
            <div class="ginput_container ginput_container_text"><input name="input_61" id="input_18_61" type="text" value="" class="medium" tabindex="43" aria-invalid="false"></div>
         </li>
         <li id="field_18_63" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: center;"><strong>VII.</strong></p>
            <p style="text-align: left;"><strong><u>CUSTODY</u></strong></p>
            Legal custody involves having basic legal responsibility for a child and making major decisions regarding the child(ren), including the child(ren)’s health, education, and religious upbringing. Rivero v. Rivero, 125 Nev.420-421, 216 P.3d 213, 221 (2009) (citing, Mack v. Ashlock, 112 Nev.1062, 1067, 921 P.2d 1258, 1262 (1996)).
            Mother and Father are fit and proper persons to be awarded <strong>Joint Legal Custody</strong> of the minor child(ren) above.
            Joint legal custody requires that the parents be able to cooperate, communicate, and compromise to act in the best interest of the child. Id. (citing, Mosely v. Figliuzzi, 113 Nev. 51, 60-61, 930 P.2d 1110, 1116 (1997)). In a joint legal custody situation, the parents must consult with each other to make major decisions regarding the child(ren)’s upbringing, while the parent with whom the child is residing at that time usually makes minor day-to-day decisions. Id. (citing, Mack, 112 Nev. at 1076, 921 P.2d at 1262).
            &nbsp;
            OR
            Mother/Father is a fit and proper person to have sole legal custody of the minor child(ren) above based upon the following:
            <p style="text-align: center;"><strong>VIII.</strong></p>
            Mother/Father are fit and proper persons to be awarded <strong>Joint Physical Custody</strong> of the minor child(ren) named above with visitation set forth in the following schedule.
            OR
            Mother/Father is a fit and proper person to have the <strong>Primary Physical Custody</strong> of the minor child(ren) above with visitation by the non-custodial parent as set forth in the following schedule.
            OR
            Mother/Father is a fit and proper person to have the <strong>Sole Physical Custody</strong> of the minor child(ren) above with visitation by the non-custodial parent as set forth in the following schedule.
         </li>
         <li id="field_18_86" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: center;"><strong>IX.</strong>
               <strong>WEEKLY/MONTHLY AND SUMMER EXCHANGE AND VISITATION</strong>
            </h3>
            <p style="text-align: left;"><strong>Visitation schedule of the minor child(ren) shall be as follows:</strong></p>
            <p style="text-align: left;"><strong>Father/Mother, will have the minor child(ren) on</strong></p>
         </li>
         <li id="field_18_64" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: center;"><strong><u>SPOUSAL SUPPORT</u></strong></h3>
            <p style="text-align: left;">Alimony and spousal support for either parties is not appropriate in this case.</p>
            <p style="text-align: left;"><strong>OR</strong></p>
         </li>
         <li id="field_18_87" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_87">Holiday Break Times:</label>
            <div class="ginput_container ginput_container_text"><input name="input_87" id="input_18_87" type="text" value="" class="medium" tabindex="44" aria-invalid="false"></div>
         </li>
         <li id="field_18_88" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_88">Spring Break:</label>
            <div class="ginput_container ginput_container_text"><input name="input_88" id="input_18_88" type="text" value="" class="medium" tabindex="45" aria-invalid="false"></div>
         </li>
         <li id="field_18_89" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_89">Winter Break:</label>
            <div class="ginput_container ginput_container_text"><input name="input_89" id="input_18_89" type="text" value="" class="medium" tabindex="46" aria-invalid="false"></div>
         </li>
         <li id="field_18_90" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_90">Summer Break:</label>
            <div class="ginput_container ginput_container_text"><input name="input_90" id="input_18_90" type="text" value="" class="medium" tabindex="47" aria-invalid="false"></div>
         </li>
         <li id="field_18_91" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: left;">Exchange of the minor child will occur on:<br>
               And transportation shall be provided by:
            </p>
            <p style="text-align: left;">The Parents may, from time to time, adjust the transportation provision or the weekends of the scheduled visitation by agreement.</p>
         </li>
         <li id="field_18_92" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h2 style="text-align: center;">X.</h2>
            <h3 style="text-align: center;"><strong>HOLIDAY SCHEDULE</strong></h3>
            <p style="text-align: left;"><strong>The holiday schedule shall be as follows:</strong></p>
         </li>
         <li id="field_18_93" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_93">Thanksgiving will be altered with Mother/Father having the child(ren) in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_93" id="input_18_93" type="text" value="" class="medium" tabindex="48" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_94" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_94">Christmas Eve will be altered with Mother/Father having the child(ren) in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_94" id="input_18_94" type="text" value="" class="medium" tabindex="49" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_95" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_95">Christmas will be altered with Mother/Father having the child(ren) in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_95" id="input_18_95" type="text" value="" class="medium" tabindex="50" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_96" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_96">Easter will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_96" id="input_18_96" type="text" value="" class="medium" tabindex="51" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_97" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_97">New Year’s Day will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_97" id="input_18_97" type="text" value="" class="medium" tabindex="52" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_98" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_98">Memorial Day will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_98" id="input_18_98" type="text" value="" class="medium" tabindex="53" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_99" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_99">Fourth of July will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_99" id="input_18_99" type="text" value="" class="medium" tabindex="54" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_100" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_100">Labor Day will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_100" id="input_18_100" type="text" value="" class="medium" tabindex="55" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_101" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_101">Nevada Day will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_101" id="input_18_101" type="text" value="" class="medium" tabindex="56" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_102" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_102">Halloween will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_102" id="input_18_102" type="text" value="" class="medium" tabindex="57" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_103" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_103">Veteran’s Day will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_103" id="input_18_103" type="text" value="" class="medium" tabindex="58" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_104" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_104">Child(ren)’s Birthday will be altered with Mother/Father having the child(ren)in the year</label>
            <div class="ginput_container ginput_container_text"><input name="input_104" id="input_18_104" type="text" value="" class="medium" tabindex="59" placeholder="Years" aria-invalid="false"></div>
            <div class="gfield_description">and each even/odd year thereafter. </div>
         </li>
         <li id="field_18_105" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <p style="text-align: left;">Mother shall have the child(ren)on Mother’s Day and Father will have the minor child(ren)on Father’s Day.
               Mother shall have the child(ren)on her birthday and Father shall have the child(ren)on his birthday.
            </p>
         </li>
         <li id="field_18_106" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">Holidays not specifically time defined shall begin that</p>
         </li>
         <li id="field_18_107" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_107"></label>
            <div class="ginput_container ginput_container_text"><input name="input_107" id="input_18_107" type="text" value="" class="small" tabindex="60" aria-invalid="false"></div>
         </li>
         <li id="field_18_108" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">of the holiday week at </p>
         </li>
         <li id="field_18_109" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_109"></label>
            <div class="ginput_container ginput_container_text"><input name="input_109" id="input_18_109" type="text" value="" class="medium" tabindex="61" aria-invalid="false"></div>
         </li>
         <li id="field_18_110" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">and end at </p>
         </li>
         <li id="field_18_113" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_113"></label>
            <div class="ginput_container ginput_container_text"><input name="input_113" id="input_18_113" type="text" value="" class="medium" tabindex="62" aria-invalid="false"></div>
         </li>
         <li id="field_18_112" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">on that  </p>
         </li>
         <li id="field_18_111" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_111"></label>
            <div class="ginput_container ginput_container_text"><input name="input_111" id="input_18_111" type="text" value="" class="medium" tabindex="63" aria-invalid="false"></div>
         </li>
         <li id="field_18_114" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            Each parent shall have a block time of time with the child(ren)for vacation purposes. That length of time for vacation period shall be: One Week/ Two Weeks
            Father/Mother shall notify the other parent, <u>in writing</u>, at least 2 weeks in advance of the choice of time for the vacation.
            &nbsp;
            Each parent shall notify the other if they take the child(ren)out of the State of Nevada for more than 72 hours, for any reason. Notification shall be made prior to leaving the State and shall include the date leaving the State, the destination, the date returning to the State, the type of transportation, and, if possible, a telephone number for contact while the child(ren)is out of the State. &nbsp;&nbsp;&nbsp; Acquiescence to travel out of state is not required by the other parent, only notification of such travel.
            Each parent shall immediately notify the other if an emergency occurs with the child(ren)such that medical treatment is needed or sought.
            Each parent shall/not keep the other informed of the childcare giver for the child(ren), including name, address and telephone number.
            Each parent shall have the right of first refusal to care for the child(ren)if the other parent is not available to care for the child(ren)for a period of 24 hours. In other words, if the child is/are in Mother’s custody and Mother is not available to care for the child for 24 hours or more, Father shall be notified and given the right of first refusal to care for the child(ren)before any third party is called in to care for the child(ren). Mother has the same right of refusal when the child(ren)is/are with Father and Father is not available to care for the child(ren)for 24 hours, or more.
            Both parents are to have equal access to all the child(ren)’s medical records, school records, and any other records generated for the benefit of, or on behalf of, the child(ren).
            <h3 style="text-align: center;"><strong>XI.</strong></h3>
            <h3 style="text-align: center;"><u>CHILD SUPPORT</u></h3>
            Mother/Father shall pay child support in the amount of $000.00 per month, per child, for a total monthly child support obligation of $000.00. The child support shall be paid on or before the first day of each month.
            The parties came to this agreement based upon the following information:
            Husband’s gross monthly income is $0,000 (amount earned per month before deductions).
            Wife’s gross monthly income is $0,000 (amount earned per month before deductions).
            Father is the non-custodial parent and, the amount agreed upon above is in compliance with NRS 125B.070 and is 18%/25%/29%/31% of Wife’s/Father’s gross monthly income or the statutory minimum amount.
            OR
            The parents are joint physical custodians, the amount of child support agreed upon meets the statutory requirement.
            OR
            The support obligation amount agreed upon by the parties is not the amount required in the statutes. Under the statutes, the child support obligation for Mother/Father would be $000.00 per month per child. However, that amount should be different based upon NRS 125B.080:
            <ul>
               <li>the cost of health insurance</li>
               <li>the cost of child care</li>
               <li>special educational needs of the child</li>
               <li>the age of the child</li>
               <li>the legal responsibility of the parents for the support of others</li>
               <li>the value of services contributed by either parent</li>
               <li>any public assistance paid to support the child</li>
               <li>any expenses reasonably relate to the mother’s pregnancy and confinement</li>
               <li>the cost of transportation of the child to and from visitation if the custodial parent moved with the child from the jurisdiction of the court which ordered the support and the noncustodial parent remained</li>
               <li>the amount of time the child spends with each parent</li>
               <li>any other necessary expenses for the benefit of the child</li>
               <li>the relative income of both parents</li>
            </ul>
            The child support obligation for each child shall continue until that child reaches the age of eighteen years, or, if the child is still attending high school at the age of eighteen years, until the child reaches the age of nineteen years or graduates from high school, or is otherwise emancipated, whichever occurs first.
            A wage assignment for the child support will/not be immediately put in place.
            There is already a Child Support action through the District Attorney’s Office and payment of the child support shall continue to be handled through that office.
            OR
            The children are receiving Welfare benefits and the Welfare Department has, or will have, a child support case through the District Attorney’s Office and the District Attorney’s Office shall continue to handle the child support payments.
            OR
            No formal child support obligation has ever previously been established and this will be the first Court Order for child support and the parent paying child support will pay the support directly to the receiving parent.
            OR
            &nbsp;
            Although this is the first Court Order for child support, the payments will be through the District Attorney’s Office and the parent who will be collecting child support shall open the case with the District Attorney’s Office.
            <h3 style="text-align: center;"><strong>XII.</strong></h3>
            <h3 style="text-align: center;"><u>HEALTH CARE</u></h3>
            The child(ren)is/not presently covered by health insurance policy.
            The child(ren)is presently not on Medicaid.
            Father/Mother shall maintain health insurance on the child through his/her employment.
            The parties shall each share, equally, any health expenses incurred on behalf of the child(ren)that are not covered by insurance, and each party shall be responsible for one half of the deductible and one half of the insurance premium.
            Mother currently pays $000.00 per month for the minor child(ren)’s medical insurance.
            Father shall pay half this expense in the amount of $000.00 per month.
            &nbsp;
            <h3 style="text-align: center;"><u>TAXES</u></h3>
            Mother shall claim the child(ren)on her taxes for 20___ and each even/odd year thereafter.
            Father shall claim the child(ren)on his taxes for 20___ and each even/odd year thereafter.
            OR
            Mother/Father shall claim the child(ren)on his/her taxes each year as she is has primary physical custody of the minor child.
            <h3 style="text-align: center;"><strong>XIII.</strong></h3>
            <h3 style="text-align: center;"><u>ASSETS</u></h3>
            The Petitioners affirmatively state that they have agreed as to the nature of the community property and division thereof of any and all community assets prior to the commencement of this action and each is to keep the property they have in their possession.
            OR
            Additionally, the community property should be divided as follows:
            WIFE shall receive the following:
            HUSBAND shall receive the following:
            &nbsp;
            <h3 style="text-align: center;"><strong>XIV.</strong></h3>
            <h3 style="text-align: center;"><u>DEBTS</u></h3>
            Petitioners affirmatively state that all of the community debts have been previously divided
            and each is to keep those debts assigned to them and indemnify and hold the other party harmless from those debts.
            OR
            Additionally, the community debts should be divided as follows:
            WIFE shall be responsible for the following:
            HUSBAND shall be responsible for the following:
            Petitioners hereby certify that they have disclosed all community assets and debts and that there are no other community assets or debts for this Court to divide.<strong>.</strong>
         </li>
         <li id="field_18_116" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <h3 style="text-align: center;"><strong>XV.</strong></h3>
            <h3 style="text-align: center;"><strong>SPOUSAL SUPPORT</strong></h3>
            <p style="text-align: center;">Alimony and spousal support for either parties is not appropriate in this case.
               OR
            </p>
         </li>
         <li id="field_18_65" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_65"></label>
            <div class="ginput_container ginput_container_text"><input name="input_65" id="input_18_65" type="text" value="" class="medium" tabindex="64" aria-invalid="false"></div>
            <div class="gfield_description">Name of wife/husband </div>
         </li>
         <li id="field_18_66" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">will receive alimony in the amount of</p>
         </li>
         <li id="field_18_67" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_67"></label>
            <div class="ginput_container ginput_container_text"><input name="input_67" id="input_18_67" type="text" value="" class="medium" tabindex="65" aria-invalid="false"></div>
            <div class="gfield_description">$XXX </div>
         </li>
         <li id="field_18_68" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_68"></label>
            <div class="ginput_container ginput_container_text"><input name="input_68" id="input_18_68" type="text" value="" class="medium" tabindex="66" aria-invalid="false"></div>
            <div class="gfield_description">Per week/month</div>
         </li>
         <li id="field_18_69" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">due on the 1st day of each month for</p>
         </li>
         <li id="field_18_70" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_70"></label>
            <div class="ginput_container ginput_container_text"><input name="input_70" id="input_18_70" type="text" value="" class="medium" tabindex="67" aria-invalid="false"></div>
            <div class="gfield_description">X months/years</div>
         </li>
         <li id="field_18_71" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible"></li>
         <li id="field_18_72" class="gfield gf_inline gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">The alimony shall begin on:</p>
         </li>
         <li id="field_18_73" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_73"></label>
            <div class="ginput_container ginput_container_text"><input name="input_73" id="input_18_73" type="text" value="" class="medium" tabindex="68" aria-invalid="false"></div>
            <div class="gfield_description">Date</div>
         </li>
         <li id="field_18_74" class="gfield gf_inline gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <br><br>
            <p style="text-align: left;">and</p>
         </li>
         <li id="field_18_75" class="gfield gf_inline field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_75"></label>
            <div class="ginput_container ginput_container_text"><input name="input_75" id="input_18_75" type="text" value="" class="medium" tabindex="69" aria-invalid="false"></div>
            <div class="gfield_description">End Date</div>
         </li>
         <li id="field_18_76" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibility_visible">
            <u>FORMER NAME OF WIFE</u>
            Wife wishes to return to her former name of:
            OR
            Wife never changed her name, and therefore, does not request a restoration of her former name.
            <p style="text-align: center;"><strong>XVII.</strong></p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The parties are incompatible in marriage and there is no hope for reconciliation, and/or the parties have lived separate and apart for more than one year without cohabitation.&nbsp; It is understood by the Petitioners that entry of Decree of Divorce constitutes a final adjudication of the rights and obligations of the parties with respect to the status of the marriage.
            Petitioners each expressly give up their respective rights to receive written Notice of Entry of any Decree and Judgment of Divorce and Petitioners give up their right to request a formal Findings of Fact and Conclusions of Law, or to appeal any Judgment or Order of this Court made and entered in these proceedings or the right to move for a new trial.
            <p style="text-align: center;"><strong>XVIII.</strong></p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It is further understood by the Petitioners that a final Decree of Divorce entered by this summary procedure does not prejudice of prevent the rights of either Petitioner to bring an action to set aside the final decree for fraud, duress, accident, mistake or the grounds recognized at law or in equity.
            WHEREFORE, Petitioners pray that the court enter an order as follows:
            <ol>
               <li>That the bonds of matrimony heretofore and now existing between the Petitioners be</li>
            </ol>
            forever dissolved, and that each of the parties be restored to the status of unmarried persons.
            <ol start="2">
               <li>That the terms agreed upon in this Joint Petition be ratified, approved, adopted and</li>
            </ol>
            included by the Court and fully merged and incorporated in the Decree.
            <ol start="3">
               <li>For other and further relief as the Court may deem just and proper in this action.</li>
            </ol>
            <u>AFFIRMATION</u>
            Pursuant to NRS 239B.030, the undersigned does hereby affirm that the preceding document does not contain the social security number of any person.
            We declare, under penalty of perjury under the law of the State of Nevada, that the foregoing is true and correct.
         </li>
         <li id="field_18_77" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_77">DATED on</label>
            <div class="ginput_container ginput_container_text"><input name="input_77" id="input_18_77" type="text" value="" class="medium" tabindex="70" aria-invalid="false"></div>
         </li>
         <li id="field_18_78" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_78">Your Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_78" id="input_18_78" type="text" value="" class="medium" tabindex="71" aria-invalid="false"></div>
         </li>
         <li id="field_18_79" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_79">Spouse’s Name</label>
            <div class="ginput_container ginput_container_text"><input name="input_79" id="input_18_79" type="text" value="" class="medium" tabindex="72" aria-invalid="false"></div>
         </li>
         <li id="field_18_80" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_80">Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_80" id="input_18_80" type="text" value="" class="medium" tabindex="73" aria-invalid="false"></div>
         </li>
         <li id="field_18_81" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_81">Address</label>
            <div class="ginput_container ginput_container_text"><input name="input_81" id="input_18_81" type="text" value="" class="medium" tabindex="74" aria-invalid="false"></div>
         </li>
         <li id="field_18_82" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_82">City, State, Zip</label>
            <div class="ginput_container ginput_container_text"><input name="input_82" id="input_18_82" type="text" value="" class="medium" tabindex="75" aria-invalid="false"></div>
         </li>
         <li id="field_18_83" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_83">City, State, Zip</label>
            <div class="ginput_container ginput_container_text"><input name="input_83" id="input_18_83" type="text" value="" class="medium" tabindex="76" aria-invalid="false"></div>
         </li>
         <li id="field_18_84" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_84">Tel</label>
            <div class="ginput_container ginput_container_phone"><input name="input_84" id="input_18_84" type="text" value="" class="medium" tabindex="77" aria-invalid="false"></div>
         </li>
         <li id="field_18_85" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibility_visible">
            <label class="gfield_label" for="input_18_85">Tel</label>
            <div class="ginput_container ginput_container_phone"><input name="input_85" id="input_18_85" type="text" value="" class="medium" tabindex="78" aria-invalid="false"></div>
         </li>
      </ul>
   </div>
   <div class="gform_footer top_label"> 
   <input type="submit" id="gform_submit_button_18" class="gform_button button" value="Submit"> 
   </div>
</form>

<?php
$result = ob_get_clean();
return $result;
//============================================================+
// END OF FILE
//============================================================+
