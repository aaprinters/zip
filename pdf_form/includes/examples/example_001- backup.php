<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

add_action( 'init', 'pdf_forms1' );
require_once('tcpdf_include.php');
$plugin_dir = ABSPATH . 'wp-content/plugins/pdf_form/includes/tcpdf.php';
///$dir1 = $dir.'/pdf_form/includes/tcpdf.php';
//echo $dir1;
require_once($plugin_dir);
//require_once('/../tcpdf.php');
function pdf_forms1() {
	
	add_shortcode( 'show_main_form', 'main_form11' );
	add_shortcode( 'show_user_qualification', 'user_qualification' );
	add_shortcode( 'show_user_registeration', 'user_registeration' );
	add_shortcode( 'show_make_payment', 'make_payment' );
}

function make_payment() {
if (isset($_POST["submit_user"])) {
$terms_agree = $_POST['terms_agree'];
$pymtMethod = $_POST['pymtMethod'];
if($terms_agree =='Y' && $pymtMethod == 'P'){
$product_id = 336;
$cart_item_key = WC()->cart->add_to_cart( $product_id);
}
}
?>
<form method="post">
<p>TERMS & CONDITIONS OF USE
Updated 10/1/2015
The Terms and Conditions of Use are as follows:

By creating an account and/or using 3StepDivorce ("the software and service"), you agree to, and acknowledge that you understand, the Terms and Conditions of Use set forth below.

IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS OF USE AND DO NOT WISH TO BE BOUND BY THESE TERMS AND CONDITIONS OF USE, DO NOT USE 3STEPDIVORCE. BY USING 3STEPDIVORCE YOU AGREE THAT YOU ARE AT LEAST 18 YEARS OF AGE, ARE LEGALLY ABLE TO ENTER INTO A CONTRACT AND AGREE TO THESE TERMS AND CONDITIONS OF USE. THESE TERMS AND CONDITIONS OF USE FORM A BINDING AGREEMENT BETWEEN YOU AND 3 STEP SOLUTIONS, LLC PLEASE NOTE THAT THIS AGREEMENT PROVIDES FOR THE MANDATORY ARBITRATION OF DISPUTES AND INCLUDES A WAIVER OF THE RIGHT TO A JURY TRIAL AND CLASS ACTION WAIVER AS SET FORTH BELOW.</p>
<tr class="valignTop"><td>Have you and youe spouse resident in nevada for the last six weeks?</td>
	<td><input type="checkbox" name="terms_agree" value="Y">Yes &nbsp;
	</td>
</tr>
<tr>
<td style="background:#ffd;width:45%;border:1px solid #ccc;padding:8px;text-align:center">
	<b>PayPal</b>
	<br>
	<input type="radio" name="pymtMethod" value="P">
	<br><img src="/divorce/images/paypalOption.gif" width="60" height="38" alt="PayPal">
        <br>1 payment of <b>$299</b>
	</td>
</tr>
   <input type="submit" name="submit_user" id="" class="gform_button button" value="Register"> 
</div>
</form>
   <?php
}
function user_registeration() {
if (isset($_POST["submit_user"])) {
$user_email= $_POST['user_email'];
$user_name= $_POST['user_name'];
$user_password = $_POST['user_password'];
$user_phone = $_POST['user_phone'];
$user_fax = $_POST['user_fax'];
$display_name = $_POST['display_name'];
$userdata = array(
    'user_login'  =>  $user_name,
    'user_email'  =>  $user_email,
    'display_name'  =>  $display_name,
    'user_pass'   =>  $user_password
);

$user_id = wp_insert_user( $userdata ) ;
update_user_meta( $user_id, 'user_phone', $user_phone);
update_user_meta( $user_id, 'user_fax', $user_fax);
//On success
if ( ! is_wp_error( $user_id ) ) {
    echo "YOur registeration has been completed Click Next to Proceed <a target='_top' href='http://mobile.logicsbuffer.com/terms/'>Next</a><span style='padding-left:8px;color:#922'></span>";
}

}
?>
<form method="post">
        <table width="100%" border="0" cellpadding="5">
            <tbody>
                <tr>
                    <td width="60%" align="left">
                        <div class="mediumHead2">
                            Create Your Account
                        </div>

                        All information is kept private and is secured through SSL 2048-bit (secure socket layer) encryption with Symantec, the leader in online security software.

                    </td>
                    <td width="20%" align="left" valign="middle">

                        <a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.3stepdivorce.com"><img width="65" height="37" src="//images.scanalert.com/meter/www.3stepdivorce.com/31.gif" alt="Secure Site"></a>
                    </td>

                </tr>
                <tr>
                    <td style="padding-top:12px" colspan="3" align="center"><img src="/agreements/images/acctHeader2.gif" width="680" height="21" alt=""></td>
                </tr>

                <tr valign="top">
                    <td colspan="3" style="padding:0 110px">
                        <span style="font-size:18px;font-weight:bold">1.</span> Invent a username and password that will be used to access your account. We will not sell, rent or lease your personally identifiable information to anyone! We respect your privacy.
                        <br>
                    </td>
                </tr>

                <tr>
                    <td class="label">Username</td>
                    <td class="input" colspan="2">

                        <input name="user_name" size="16" value="">
                        <span class="tiny">(This should be a one word name with no spaces.)</span>

                    </td>
                </tr>

                <!-- NEW -->

                <tr>
                    <td class="label">Password</td>
                    <td class="input" colspan="2">
                        <input type="password" name="user_password" size="32" maxlength="32" autocomplete="off" value="">

                    </td>
                </tr>
                <tr>
                    <td class="label" nowrap="">Re-enter Password</td>
                    <td class="input" colspan="2">
                        <input type="password" name="password2" size="32" autocomplete="off" value="">

                    </td>
                </tr>

                <tr>
                    <td class="label">Email address</td>
                    <td class="input" colspan="2">
                        <input name="user_email" size="32" value="">
                    </td>
                </tr>
                <tr>
                    <td class="label">Phone</td>
                    <td class="input" colspan="2">
                        <input name="user_phone" size="15" value="">
                        <span class="tiny">(Optional)</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <b>Fax</b>
                        <input name="user_fax" size="15" value="">
                        <span class="tiny">(Optional)</span></td>
                </tr> 
				<tr>
				<td class="label">Your Name</td>
					<td class="input" colspan="2">

						<input name="display_name" size="16" value="">

					</td>
				</tr> 
            </tbody>
        </table>
		<div class="qualifyContinue">
   <input type="submit" name="submit_user" id="" class="gform_button button" value="Register"> 
</div>
</form>
   <?php
   }
   
function user_qualification() {
// create new PDF document
if (isset($_POST["submit_qualify"])) {
$residence= $_POST['residence'];
$county= $_POST['county'];
$locateSpouse = $_POST['locateSpouse'];
$children = $_POST['children'];
$agree = $_POST['agree'];
$military = $_POST['military'];
$resident = $_POST['resident'];
if($residence=='Nevada' && $locateSpouse=='Y' && $children=='Y' && $agree=='Y' && $resident=='Y'){
?>
<div style="background:#FFF">
<table>
<tbody><tr>
<td class="bigHead">CONGRATULATIONS You Qualify. Continue Below.</td>
</tr></tbody></table>

<div class="blueBox">
<div class="blueBoxTop"></div>
<div class="blueBoxContent">
<b>To start the process of completing your Nevada divorce forms online, please click "Continue" below.</b>
<ul>
<li>Save 100s of Dollars by Doing Your Own Divorce.</li>
<li>We Save Your Work as You Go.</li>
<li>Instant Delivery of Your Nevada Documents.</li>
<li>Instant Changes &amp; Updates.</li>
<li>Court Approval or Your Money Back.</li>
<li>Secure &amp; Private - Tested Daily.</li>
</ul>
</div><!-- End of blueBoxContent-->
<div class="blueBoxBottom"></div>
</div><!-- End of blueBox -->
<br>
<table class="qualifyYes">
<tbody><tr><td class="step2q"></td>
<td class="mediumHead"><a target="_top" href="http://mobile.logicsbuffer.com/registeration/">Next</a><span style="padding-left:8px;color:#922"></span></td>
<td style="text-align:right"><a target="_top" href="http://mobile.logicsbuffer.com/registeration/"><img src="http://mobile.logicsbuffer.com/wp-content/plugins/pdf_form/includes/include/continueButton2.png" alt="Continue"></a></td>
</tr>
</tbody></table>
</div>
<?php
}else{
	echo'<p style="background:#FFF">
Note: We are sorry you must be able to locate your spouse and you both must agree on the terms of divorce.
Easy NV divorce may not be the best solution. Please call for additional
assistance phone XXX-XXX-XXXX or contact a local attorney for legal advise.
</p>';	
}
}
?>
<div style="background:none #fff">
<table>
<tbody><tr><td class="step1q2"> </td>
<td class="bigHead">Do you qualify for our Nevada divorce online?</td>
</tr>
</tbody></table>

<form method="post" action="">
<div class="blueBox">
<div class="blueBoxTop"></div>
<div class="blueBoxContent">
<table class="blueBoxForm pad6">
<tbody>
<tr>
<td>State of Residence:</td>
<td><select name="residence">
<option value="">Choose State</option>
<option value="/states/alabama.shtml">Alabama</option>
<option value="Nevada" selected="selected">Nevada</option>
<option value="/states/arizona.shtml">Arizona</option>
<option value="/states/arkansas.shtml">Arkansas</option> 
<option value="/states/california.shtml">California</option>
<option value="/states/colorado.shtml">Colorado</option>
<option value="/states/connecticut.shtml">Connecticut</option>
<option value="/states/delaware.shtml">Delaware</option>
<option value="/states/dc.shtml">Wash. DC</option>
<option value="/states/florida.shtml">Florida</option>
<option value="/states/georgia.shtml">Georgia</option>
<option value="/states/hawaii.shtml">Hawaii</option>
<option value="/states/idaho.shtml">Idaho</option>
<option value="/states/illinois.shtml">Illinois</option>
<option value="/states/indiana.shtml">Indiana</option> 
<option value="/states/iowa.shtml">Iowa</option>
<option value="/states/kansas.shtml">Kansas</option>
<option value="/states/kentucky.shtml">Kentucky</option> 
<option value="/states/louisiana.shtml">Louisiana</option>
<option value="/states/massachusetts.shtml">Massachusetts</option>
<option value="/states/maryland.shtml">Maryland</option> 
<option value="/states/maine.shtml">Maine</option>
<option value="/states/michigan.shtml">Michigan</option>
<option value="/states/minnesota.shtml">Minnesota</option>
<option value="/states/mississippi.shtml">Mississippi</option>
<option value="/states/missouri.shtml">Missouri</option>   
<option value="/states/montana.shtml">Montana</option> 
<option value="/states/nebraska.shtml">Nebraska</option>
<option value="/states/nevada.shtml">Nevada</option>
<option value="/states/newhampshire.shtml">New Hampshire</option> 
<option value="/states/newjersey.shtml">New Jersey</option>
<option value="/states/newmexico.shtml">New Mexico</option>
<option value="/states/newyork.shtml">New York</option>
<option value="/states/northcarolina.shtml">North Carolina</option>
<option value="/states/northdakota.shtml">North Dakota</option> 
<option value="/states/ohio.shtml">Ohio</option>
<option value="/states/oklahoma.shtml">Oklahoma</option>
<option value="/states/oregon.shtml">Oregon</option>
<option value="/states/pennsylvania.shtml">Pennsylvania</option>
<option value="/states/rhodeisland.shtml">Rhode Island</option>
<option value="/states/southcarolina.shtml">South Carolina</option>
<option value="/states/southdakota.shtml">South Dakota</option> 
<option value="/states/tennessee.shtml">Tennessee</option> 
<option value="/states/texas.shtml">Texas</option>
<option value="/states/utah.shtml">Utah</option>
<option value="/states/vermont.shtml">Vermont</option>
<option value="/states/virginia.shtml">Virginia</option>
<option value="/states/washington.shtml">Washington</option>
<option value="/states/westvirginia.shtml">West Virginia</option>
<option value="/states/wisconsin.shtml">Wisconsin</option> 
<option value="/states/wyoming.shtml">Wyoming</option>
</select>
</td>
</tr>
<tr>
<td>County in Nevada:</td>
<td><select name="county">
<option value="">Choose County</option>
<option value="carson_city">Carson City</option>

<option value="churchill_county" selected="selected">Churchill County</option>

<option value="clark_county" selected="selected">Clark County</option>

<option value="douglas_county" selected="selected">Douglas County</option>

<option value="elko_county" selected="selected">Elko County</option>

<option value="esmeralda_county" selected="selected">Esmeralda County</option>

<option value="eureka_county" selected="selected">Eureka County</option>

<option value="humboldt_county" selected="selected">Humboldt County</option>

<option value="lander_county" selected="selected">Lander County</option>
	
<option value="lincoln_county" selected="selected">Lincoln County</option>

<option value="lyon_county" selected="selected">Lyon County</option>

<option value="mineral_county" selected="selected">Mineral County</option>

<option value="nye_county" selected="selected">Nye County</option>

<option value="pershing_county" selected="selected">Pershing County</option>

<option value="storey_county" selected="selected">Storey County</option>

<option value="washoe_county" selected="selected">Washoe County</option>

<option value="white_pine_county" selected="selected">White Pine County</option>
</select>
</td>
</tr>
<tr><td>Can you locate your spouse?</td>
	<td style="white-space:nowrap"><input type="radio" name="locateSpouse" value="Y" checked="checked">Yes &nbsp;
	    <input type="radio" name="locateSpouse" value="N">No
	</td>
</tr>
<tr class="valignTop"><td>Do you have children under the age of 18 from this marriage?</td>
	<td><input type="radio" name="children" value="Y" checked="checked">Yes &nbsp;
	    <input type="radio" name="children" value="N">No
	</td>
</tr>
<tr class="valignTop"><td>Do you and your spouse agree to the issues regarding your divorce?</td>
	<td><input type="radio" name="agree" value="Y" checked="checked">Yes &nbsp;
	    <input type="radio" name="agree" value="N">No
	</td>
</tr>
<tr class="valignTop"><td>Are you and/or your spouse active duty military?</td>
	<td><input type="radio" name="military" value="Y" checked="checked">Yes &nbsp;
	    <input type="radio" name="military" value="N">No
	</td>
</tr>
<tr class="valignTop"><td>Have you and youe spouse resident in nevada for the last six weeks?</td>
	<td><input type="radio" name="resident" value="Y" checked="checked">Yes &nbsp;
	    <input type="radio" name="resident" value="N">No
	</td>
</tr>

</tbody></table>
</div><!-- End of blueBoxContent-->
<div class="blueBoxBottom"></div>
</div><!-- End of blueBox -->
<table class="arrowLinkLeft">
<tbody><tr><td><a target="_top" href="/createaccount.shtml"><img src="/img/bullets.png" width="14" height="34" alt=""></a></td>
    <td><a target="_top" href="/createaccount.shtml">Need a different state</a>?<br><a target="_top" href="/separation/separationagreement.shtml">Not Ready - Need Your Separation Agreement</a>?</td>
</tr>
</tbody></table>
<div class="qualifyContinue">
   <input type="submit" name="submit_qualify" id="gform_submit_button_18" class="gform_button button" value="Submit"> 
</div>
</form>
</div>
<?php
}
function main_form11() {
// create new PDF document
// create new PDF document
if (isset($_POST["submit_info"])) {
		$user_name= $_POST['input_name'];
		$spouse_name= $_POST['input_spouse'];
		$date_of_marraige= $_POST['input_dom'];
		$case_num= $_POST['input_case_num'];
		$dept_num= $_POST['input_dept_num'];
		$city= $_POST['input_city'];
		$county= $_POST['input_county'];
		$state= $_POST['input_state'];
		$address= $_POST['input_address'];
		$zip= $_POST['input_zip'];
		$tel= $_POST['input_phone'];
}

// pdf1 Code from 1
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 002');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', 'BI', 12);

// add a page
$pdf->AddPage();

// set some text to print
$name = 'asdasdada';

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// Set some content to print
$html = '
<div class="container">
<h3>CODE:</h3>
<h3 style="text-align: center; font-weight: bold;">IN THE FAMILY DIVISION <br>

OF THE SECOND JUDICIAL DISTRICT COURT OF THE STATE OF NEVADA<br>

IN AND FOR THE COUNTY OF WASHOE</h3>	

	<div class="row">
		
		<div class="col" style="width:50%;float:left">
			In the Matter of the Marriage of <br>'.$user_name.'and<br>'.$spouse_name.'
		</div>
		<div class="col" style="font-size:12px; width:50%;float:left">
			<p>Case No. '.$case_num.'</p>
			<p>Dept. No. '.$dept_num.'</p>
		</div>
	</div>

	<div class="col">
		<h3 style="text-align: center;">DECREE OF DIVORCE <br>
WITH CHILD(REN)</h3>
	</div>
		
		<div class="row">
			<div class="col">This matter having been submitted 		to the Court by the Petitioners '.$user_name.' and <br> '.$spouse_name.', in Pro Per, 		and pursuant to Chapter 125 of the Nevada Revised Statutes, <br> for a
				Summary Decree of Divorce, and based upon the Joint Petition by Petitioners and all of the papers
					and pleadings on file, the Court finds as follows:
			</div>
			<ol>
		   <li>That all of the allegations contained in the documents on file are true.</li>
            <li>That all of the requirements of NRS 125.181 and NRS 125.182 have been met.</li>
            <li>That this Court does/ does not have the necessary UCCJA, UCCJEA and PKPA initial and continuing jurisdiction to enter orders regarding child custody and visitation on the following child(ren) of the union or adopted by the parties, and hereby exercises said jurisdiction OR and said issues must be decided in the child(ren)’s present “home state”:</li>
				</ol>			
			<ol start="4">
            <li>That this Court has complete jurisdiction to enter this Decree and the orders regarding the distribution of assets and debts.</li>
            <li>That resident Petitioner '.$user_name.' has been, and is now, an actual bona fide resident of the State of Nevada and has actually been domiciled in the State of Nevada for more than six (6) weeks immediately prior to the commencement of this action and intends to continue to make the State of Nevada their home for an indefinite period of time.</li>
            <li>The parties were married on '.$date_of_marraige.', in the city of '.$city.', County '.$county.', State of '.$state.', and have ever since that date been and still are Husband and Wife.</li>
            <li>The parties are incompatible in their marriage, and reconciliation is not possible.</li>
            <li>That the Wife, '.$user_name.' is/not presently pregnant at this time. Husband is/not alleged to be the father of the unborn child. The unborn child is due to be born on Date.</li>
            <li>That Petitioners have entered into a Marital Settlement Agreement settling all issues regarding the care, custody, visitation, health insurance and child support of the child(ren) over whom this Court has jurisdiction, said Agreement being in the best interests of the child(ren) and Petitioners have requested that their Agreement, as set forth in the Joint Petition, be ratified, confirmed and incorporated into this Decree as though fully set forth herein.</li>
            <li>That Petitioners have entered into an Agreement settling all issues regarding the division and distribution of assets and debts, said Agreement being an equitable one, and Petitioners have requested that their Agreement as set forth in the Joint Petition be ratified, confirmed and incorporated into this Decree as though fully set forth herein.</li>
            <li>That Petitioners have entered into an Agreement settling the issue of spousal support and request that their Agreement as set forth in the Joint Petition be ratified, confirmed and incorporated into this Decree as though fully set forth herein.</li>
            <li>Wife does/not wish to return to her former name of '.$user_name.'. OR Wife never changed her name so does not request restoration of a former name.</li>
            <li>That Petitioners waive their rights to a written Notice of Entry of Decree of Divorce, to appeal, to Findings of Fact and Conclusions or Law and to move for a new trial by the Court entering of a Summary Divorce Decree with Child(ren).</li>
        </ol>
        THEREFORE, IT IS ORDERED, ADJUDGED AND DECREED:
        <ol>
            <li>On the grounds of incompatibility, the bonds of marriage now existing between the parties is hereby dissolved and an absolute Decree of Divorce is granted to the parties, and each of them are restored to the status of single, un-married persons.</li>
            <li>That their Agreement, as stated in the Petitioners’ Joint Petition regarding the care, custody, visitation, health insurance and child support of the child(ren) over which this Court has jurisdiction, is hereby ratified, confirmed and incorporated into this Decree as though fully set forth.</li>
            <li>That the Marital Settlement Agreement, as stated in the Petitioners’ Joint Petition regarding the issue of spousal support, is hereby ratified, confirmed and incorporated into this Decree as though fully set forth.</li>
            <li>Wife shall retain her present name.</li>
        </ol>
        Wife is hereby restored to her former name of '.$user_name.'. OR Wife never changed her name so does not request restoration of a former name.

        <div style="font-weight: bold;">IT IS FURTHER ORDERED AND PETITIONERS ARE PUT ON NOTICE</div> that they are subject to the requirements of the following Nevada Revised Statutes:

        <strong>            NRS 125.510(6) regarding abduction, concealment or detention of a child. </strong>

        <strong><u>PENALTY FOR VIOLATION OF ORDER</u></strong> THE ABDUCTION, CONCEALMENT OR DETENTION OF A CHILD IN VIOLATION OF THIS ORDER IS PUNISHABLE AS A CATEGORY D FELONY AS PROVIDED IN NRS 193.130. NRS 200.359 provides that every person having a limited right of custody to a child or any parent having no right of custody to the child who willfully detains, conceals or removes the child from a parent, guardian or other person having lawful custody or a right of visitation of the child in violation of an order of this court, or removes the child from the jurisdiction of the court without the consent of either the court or all persons who have the right to custody or visitation is subject to being punished for a category D felony as provided in NRS 193.130. &nbsp;

        <strong>NOTICE IS HEREBY GIVEN </strong>that the terms of the Hague Convention of October 25, 1980, adopted by the 14<sup>th</sup> Session of the Hague Conference on Private International Law, apply if a parent abducts or wrongfully retains a child in a foreign country. The parties are also put on notice of the following provisions in NRS 125.510(8): If a parent of the child lives in a foreign country or has significant commitments in a foreign country: (a) The parties may agree, and the court shall include in the order for custody of the child, that the United States is the country of habitual residence of the child for the purposes of applying the terms of the Hague Convention as set forth in subsection 7. (b) Upon motion of one of the parties, the court may order the parent to post a bond if the court determines that the parent poses an imminent risk of wrongfully removing or concealing the child outside the country of habitual residence. The bond must be in an amount determined by the court and may be used only to pay for the cost of locating the child and returning the child to his or her habitual residence if the child is wrongfully removed from or concealed outside the country of habitual residence. The fact that a parent has significant commitments in a foreign country does not create a presumption that the parent poses an imminent risk of wrongfully removing or concealing the child. &nbsp;

        <strong>NRS 125C.200 regarding relocation with minor children </strong> If custody has been established and the custodial parent intends to move his or her residence to a place outside of this State and to take the child with him or her, the custodial parent must, as soon as possible and before the planned move, attempt to obtain the written consent of the noncustodial parent to move the child from this State. If the noncustodial parent refuses to give that consent, the custodial parent shall, before leaving this State with the child, petition the court for permission to move the child. The failure of a parent to comply with the provisions of this section may be considered as a factor if a change of custody is requested by the noncustodial parent. &nbsp;

        <strong>NRS 125.450</strong> <strong>regarding the collection of child support payments through mandatory </strong>

        <strong>            wage withholding or assignment of income. </strong>

        <strong>NRS 31A regarding the enforcement of a child support obligation and the collection of delinquent child support. </strong>

        <strong> </strong>

        <strong>NRS 125B.145 regarding the review of child support at any time due to changed circumstances and at least every three years following the entry of the child support order. </strong>

        <strong> </strong>

        <strong>THIS IS A FINAL DECREE</strong>

        <strong>            </strong>Pursuant to NRS 239B.030,<strong> t</strong>he undersigned person does affirm that this document does not contain the social security number of any person. &nbsp; Dated this <u>                   </u> day of <u>                                    </u>, 20______. &nbsp;

        <u>                                                                        </u> DISTRICT JUDGE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Respectfully Submitted: &nbsp; __________________________ '.$user_name.' Your '.$address.' '.$city.', '.$state.', '.$zip.' '.$tel.': (     ) &nbsp; __________________________ '.$spouse_name.' '.$address.' '.$city.', '.$state.', '.$zip.' '.$tel.': (   )

        <em> </em> &nbsp; &nbsp;

        <strong><u>CERTIFICATE OF SERVICE</u></strong> &nbsp; I am a resident of the State of Nevada, over the age of eighteen years, and on the _____ day of <u>                            </u>, 20_______, I served the preceeding document(s):

        <strong><u> </u></strong>

        <strong><u>DECREE</u></strong>

        <strong><u> </u></strong>

        <strong>BY ELECTRONIC MEANS:</strong> by transmitting via electronic means the document(s) listed above to the fax number(s) set forth below on this date before 5:00 p.m. NRCP 5(b)(2)(D).

        <strong>BY HAND:</strong> by personally delivering the document(s) listed above to the person(s) at the address(es) set forth below. NRCP 5(b)(2)(A). &nbsp;

        <strong>BY MAIL:</strong> by placing the document(s) listed above in a sealed envelope with postage thereon fully prepaid, in the United States mail at Reno, Nevada addressed as set forth below. NRCP 5(b)(2)(B).

        <strong> </strong>

        <strong>BY DEPOSITING WITH THE CLERK:</strong> by causing document(s) to be deposited with the Clerk of the Court, as the party or their attorney has no known address.  NRCP 5(b)(2)(C) &nbsp;

        <strong>BY PERSONAL DELIVERY:</strong> by causing personal delivery by Reno/Carson Messenger Service of the document(s) listed above to the person(s) at the address(es) set forth below. &nbsp; '.$user_name.' '.$address.' '.$address.' &nbsp; '.$spouse_name.' '.$address.' '.$address.' &nbsp; &nbsp; I declare under penalty of perjury under the laws of the State of Nevada that the above is true and correct. &nbsp; Executed on <u>                                                   </u>, ________________, Nevada. &nbsp; &nbsp; &nbsp; ________________________________ &nbsp; &nbsp;

    </li>
</ol>

		</div>
</div>';

// pdf2 Code from 5
$pdf2 = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf2->SetCreator(PDF_CREATOR);
$pdf2->SetAuthor('Nicola Asuni');
$pdf2->SetTitle('TCPDF Example 002');
$pdf2->SetSubject('TCPDF Tutorial');
$pdf2->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf2->setPrintHeader(false);
$pdf2->setPrintFooter(false);

// set default monospaced font
$pdf2->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf2->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf2->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf2->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf2->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf2->SetFont('dejavusans', 'BI', 12);

// add a page
$pdf2->AddPage();

// set some text to print

// set text shadow effect
$pdf2->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html2 = '<div class="row">
	<div class="col" style="width: 100%; margin: 30px;">
CODE: '.$code.'<br>
'.$user_name.' <br>
'.$address.'
'.$city.', '.$state.', '.$zip.' <br>
'.$tel.': (&nbsp;&nbsp; ) <br>
In Pro Per  <br>
Spouse’s Name : '.$spouse_name.'  <br>
'.$address.'  <br>
'.$city.', '.$state.', '.$zip.'  <br>
Tel: ( '.$tel.')<br>
In Pro Per  <br>

<div class="row">
	<div class="col" align="center">
		<strong>IN THE FAMILY DIVISION</strong> <br>
		<strong>OF THE SECOND JUDICIAL DISTRICT COURT OF THE STATE OF NEVADA</strong> <br>
		<strong>IN AND FOR THE COUNTY OF WASHOE</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  <br> 
	</div>
</div>

<div class="row">
	<div class="col" style="width: 50%;  float: left;">		
		In the Matter of the Marriage of <br>
		'.$wife_name.' and <br>
		'.$husband_name.'
	</div>
	<div class="col" style="width: 50%; float: right;">
		Case no: '.$case_num.'<br>
		Dept no: '.$dept_num.'<br>
		<div>Joint Petitioners.
			________________________________________/
		</div>
	</div>
</div>

		
		<div class="row">
			<div class="col" style="width: 100%; float: right; text-align: center;">
				<h3>REQUEST FOR SUBMISSION</h3>
			</div>
		</div>	

		<div class="row">
			<div class="col" style="width: 100%;">
				<p>Petitioners, '.$user_name.' and <br> '.$spouse_name.', in pro per, hereby respectfully submits <br> the Joint Petition for Summary Decree of Divorce previously filed in this case for decision.
				The undersigned does affirm that pursuant to NRS 239B.030, this document does not
				contain the social security number of any person.
			</p>
			</div>
		</div>	 
			

		</div>
		

		<div class="row">
			<div class="col" style="width: 100%">
				DATED this day of _________________, 20_______.
			</div>
		</div>	

		<div class="row">
			<div class="col" style="width: 100%; float: left; text-align: right;">
				<div class="" style="padding-right: 10px;">
				<p>__________________________</p> 
				<p>'.$user_name.'</p>
				<p>'.$address.'</p>
				<p>'.$city.', '.$state.', '.$zip.'</p>
				<p>__________________________ </p>
				<p>'.$spouse_name.'</p>
				<p>'.$address.' </p> 
				<p>'.$city.', '.$state.', '.$zip.'</p> 
			</div>
			</div>
		</div>	
	</div>';
// ---------------------------------------------------------
// pdf5 Code from 5
$pdf5 = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf5->SetCreator(PDF_CREATOR);
$pdf5->SetAuthor('Nicola Asuni');
$pdf5->SetTitle('TCPDF Example 002');
$pdf5->SetSubject('TCPDF Tutorial');
$pdf5->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf5->setPrintHeader(false);
$pdf5->setPrintFooter(false);

// set default monospaced font
$pdf5->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf5->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf5->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf5->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf5->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf5->SetFont('dejavusans', 'BI', 12);

// add a page
$pdf5->AddPage();

// set some text to print

// set text shadow effect
$pdf5->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html5 = '<div class="container">
<div class="row">
	<div class="col" style="width: 100%; margin: 30px;">
CODE: '.$code.'<br>
'.$user_name.' <br>
'.$address.'.
'.$city.', '.$state.', '.$zip.' <br>
'.$tel.': (   ) <br>
In Pro Per  <br>
Spouse’s Name : '.$spouse_name.'  <br>
'.$address.'  <br>
'.$city.', '.$state.', '.$zip.'  <br>
Tel: ( '.$tel.')<br>
In Pro Per  <br>

</div>

</div>
<div class="row">
	<div class="col" style="text-align: center;">
		<div>IN THE FAMILY DIVISION</div> <br>
		<div>OF THE SECOND JUDICIAL DISTRICT COURT OF THE STATE OF NEVADA</div> <br>
		<div>IN AND FOR THE COUNTY OF WASHOE</div>
</div>
</div>

<div class="row">
<p>'.$user_name.'</p>
<p style="text-align: center;">Joint Petitioner,</p>
<p>'.$spouse_name.'</p>
									
<div class="col" style="" >
<div style="text-align: right;">
		Case no: '.$case_num.'<br>
		Dept no: '.$dept_num.'<br>
		</div>
		<div style="text-align: center;">
		Joint Petitioners.
			________________________________________/
		</div>
</div>
<div class="row">
			<div class="col" style="width: 100%; text-align: center;">
				<div>DECLARATION UNDER UNIFORM CHILD CUSTODY JURISDICTION
AND ENFORCEMENT ACT (UCCJEA)
</div>
			</div>
		</div>	

		<div class="row">
			<div class="col" style="width: 100%;">
				<p>Petitioners, '.$user_name.' and <br> '.$spouse_name.', in pro per, hereby respectfully submits <br> the Joint Petition for Summary Decree of Divorce previously filed in this case for decision.
				The undersigned does affirm that pursuant to NRS 239B.030, this document does not
				contain the social security number of any person.
			</p>
			</div>
		</div>		
		
		<div class="row">
			<div class="col">
				This document is submitted by Joint Petitioners: '.$user_name.'/ '.$spouse_name.'. 

				I declare as follows:

				I.
				For each child under the age of 18, born to, or adopted by, the parents at any time during their relationship, list where the child currently lives, where the child has lived for the past 5 years, and the names and current addresses of the persons with whom the child lived at each address. 

				If there is more than one child, and the information is the same for each child, please write, 

				“same as above” in the space provided for the child’s address, person with whom the child lived, and relationship. You must still provide information regarding each child’s name, date of birth, and gender. 

			</div>
		</div>
		<div style="text-align: center;">
				<p>CHILDREN BORN TO THIS MARRIAGE OR RELATIONSHIP</p>
				<p>********************************************************** </p>
				<p style="display: inline; padding: 0px 5px;border-bottom:2px solid #000;">	CHILD ONE </p>
				
		</div>
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Name</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Date of Birth/Age</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Male/Female</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Period of</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Child’s '.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">With Whom Child Resides Relation
				Residence</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">To Present:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Address</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Mother</span>
			
				<br>
	
				<span style="display: block;
				width: 33.3%;
				float: left;">To:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">'.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Father</span>
			
				<br>
				
				<p>
					If the child is presently residing with adults other than the parents, please state who the adults are<br> and their relationship to the child.				
				</p>
				
								<p style="display: inline; padding: 0px 5px;border-bottom:2px solid #000;">	CHILD TWO </p>
				
		</div>
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Name</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Date of Birth/Age</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Male/Female</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Period of</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Child’s '.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">With Whom Child Resides Relation
				Residence</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">To Present:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">'.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Mother</span>
			
				<br>
	
				<span style="display: block;
				width: 33.3%;
				float: left;">To:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">'.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Father</span>
			
				<br>
				
				<p>
					If the child is presently residing with adults other than the parents, please state who the adults are<br> and their relationship to the child.				
				</p>
				
				
								<p style="display: inline; padding: 0px 5px;border-bottom:2px solid #000;">	CHILD THREE </p>
				
		</div>
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Name</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Date of Birth/Age</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Male/Female</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">Period of</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">Child’s '.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">With Whom Child Resides Relation
				Residence</span>
				<hr style="padding: 0px 5px;border-bottom:1px solid #000;">
				
				<br>
				<span style="display: block;
				width: 33.3%;
				float: left;">To Present:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">'.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Mother</span>
			
				<br>
	
				<span style="display: block;
				width: 33.3%;
				float: left;">To:</span>
				<span style="display: block;
				width: 33.3%;
				float: left;text-align: center;">'.$address.'</span>
				<span style="display: block;
				width: 33.4%;
				float: left;text-align: right;">Father</span>
			
				<br>
				
				<p>
					If the child is presently residing with adults other than the parents, please state who the adults are<br> and their relationship to the child.				
				</p>
				
				<div class="row">
					<div class="col">
						<p>
						Please identify any other court case in which you have participated as a party, witness, or in any other way concerning the custody of or visitation with the child(ren) listed above?
						<p>
						<p>
						________ NO	________ YES </p>
						
						<p> If yes, child involved: </p>
						<p>Court: '.$court.'
						<p>Case No.:	'.$case_num.'</p>
						<p>Date of Custody determination:</p>
					</div>	
				</div>
				
				<p>
				Please identify any other court case in which you have participated as a party, witness, or in any other way concerning the custody of or visitation with the child(ren) listed above?
				</p>
				<p>________ NO	________ YES </p>
				
				<p> If yes, child involved: </p>
				<p>Court: '.$court.'</p>
				<p>Case No.:	'.$case_num.'</p>
				<p>Date of Custody determination:</p>
				
				<div class="row">
						<div class="col">
							<p>
								Please identify the names and addresses of any person(s) not a party to this court case who has physical custody of the child(ren) or claims rights of legal custody or physical custody of, or visitation with, the child(ren)? 
							</p>
							<p> 
								Name(s) of child(ren): 
							</p>
							<p>
								Name and address of person(s) claiming custody or visitation rights:
							</p>
							<p>
								The undersigned does hereby affirm that this document does not contain the Social Security Number of any person.
							</p>
							
							<p>
								I declare, under penalty of perjury under the laws of the State of Nevada, that the foregoing is true and correct.

							</p>
							
						</div>
						
						<div>
						DATED this_________________ day of _________________, 20_______.
						</div>
				</div>
				
				<div class="row">
					<div class="col" style="width:50%; float:left;">
					<p>__________________________</p>
					<p>'.$user_name.'</p>
					<p>'.$address.'</p>
					<p>'.$address.'</p>
					<p>Tel:</p>
					<p>In Pro Per</p>
					</div>
					
					<div class="col" style="width:50%; float:right;">
					<p>__________________________</p>
					<p>'.$spouse_name.'</p>
					<p>'.$address.'</p>
					<p>'.$address.'</p>
					<p>Tel:</p>
					<p>In Pro Per</p>

					</div>	
					
				</div>';

// ---------------------------------------------------------
// pdf3 Code from 5
$pdf3 = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf3->SetCreator(PDF_CREATOR);
$pdf3->SetAuthor('Nicola Asuni');
$pdf3->SetTitle('TCPDF Example 002');
$pdf3->SetSubject('TCPDF Tutorial');
$pdf3->SetKeywords('TCPDF, PDF, example, test, guide');

// remove default header/footer
$pdf3->setPrintHeader(false);
$pdf3->setPrintFooter(false);

// set default monospaced font
$pdf3->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf3->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf3->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf3->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf3->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf3->SetFont('dejavusans', 'BI', 12);

// add a page
$pdf3->AddPage();

// set some text to print

// set text shadow effect
$pdf3->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$html3 = '<h3>CODE: $code</h3>
<h3 style="text-align: center; font-weight: bold;">IN THE FAMILY DIVISION <br>

OF THE SECOND JUDICIAL DISTRICT COURT OF THE STATE OF NEVADA<br>

IN AND FOR THE COUNTY OF WASHOE</h3>	

	<div class="row">
		
		<div class="col" style="width:50%;float:left">
			In the Matter of the Marriage of<br>'.$user_name.'and '.$spouse_name.'

			
		</div>

		<div class="col" style="width:50%;float:left">
			In the Matter of the Marriage of	<br>

			<p>Case No. $case_num;</p>
			<p>Dept. No. $dept_num;</p>
		</div>
	</div>

	<div class="col">
		<h3 style="text-align: center;">DECREE OF DIVORCE <br>
WITH CHILD(REN)</h3>
	</div>
		
		<div class="row">
			<div class="col" style="width: 100%;margin: 10px 85px;text-align: center;">This matter having been submitted 		to the Court by the Petitioners '.$user_name.' and <br> '.$spouse_name.', in Pro Per, 		and pursuant to Chapter 125 of the Nevada Revised Statutes, <br> for a
				Summary Decree of Divorce, and based upon the Joint Petition by Petitioners and all of the papers
					and pleadings on file, the Court finds as follows:
			</div>
			<div class="col" style="width: 50%; float: right;">
				<ol>
			 			<li>That all of the allegations contained in the documents on file are true.</li>
			 			<li>That all of the requirements of NRS 125.181 and NRS 125.182 have been met.</li>
			 			<li>That this Court does/ does not have the necessary UCCJA, UCCJEA and PKPA</li>
				</ol>
			</div>
		</div>';
$dir = plugin_dir_path( __FILE__ );
$filename1 = 'file1.pdf';
$filename2 = 'file2.pdf';
$filename3 = 'file3.pdf';
$filename5 = 'file5.pdf';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf2->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);
$pdf3->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
$pdf5->writeHTMLCell(0, 0, '', '', $html5, 0, 1, 0, true, '', true);

// Create the full path
$full_path1 = $dir . '/' . $filename1;
$full_path2 = $dir . '/' . $filename2;
$full_path3 = $dir . '/' . $filename3;
$full_path5 = $dir . '/' . $filename5;
// Output PDF
$pdf->Output($full_path1, 'F');
// Output2 PDF
$pdf2->Output($full_path2, 'F');
$pdf3->Output($full_path3, 'F');
$pdf5->Output($full_path5, 'F');

print_r('http://easynvdivorce.com/dev/wp-content/plugins/pdf_form/includes/examples/file22.pdf');

//============================================================+
// END OF FILE
//============================================================+

ob_start();
?>

<div class="container form-style-5">
<form method="post">

<div class="row">
<h2>Wife INFORMATION </h2>
<div class="col">
   <div class="">
      <div class="question">
         <label>Wife's Full Name?</label>
         <input type="text" name="wife_name" size="30" value="">
      </div>
   </div>
   <div class="">
      <div class="question">
         <label>Wife's Maiden Name?</label>
         <input type="text" name="wife_maiden_name" size="30" value="">
      </div>
   </div>
   <p>Full legal name. EasyNVDDivorce</p>
  </div>
  </div>
   <div class="">
      <h4> 
         Wife's Address
      </h4>
	  <label for="wife_nv">Is wife is the resident of Nevada ?</label>
	  <input type="checkbox" id="wife_resident_nev" name="wife_coun_res">
	<div id="show_wife_resident_nev">
		<div>
         <label for="wife_res_date1">Date Wife has been a resident:</label>
         <div id="answerControls">
            <select name="wiferes_Month">
               <option value="">Select Month</option>
               <option value="January">January</option>
               <option value="February">February</option>
               <option value="March">March</option>
               <option value="April">April</option>
               <option value="May">May</option>
               <option value="June">June</option>
               <option value="July">July</option>
               <option value="August">August</option>
               <option value="September">September</option>
               <option value="October">October</option>
               <option value="November">November</option>
               <option value="December">December</option>
            </select>
            <select name="wiferes_Day">
               <option value="">Select Day</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               <option value="16">16</option>
               <option value="17">17</option>
               <option value="18">18</option>
               <option value="19">19</option>
               <option value="20">20</option>
               <option value="21">21</option>
               <option value="22">22</option>
               <option value="23">23</option>
               <option value="24">24</option>
               <option value="25">25</option>
               <option value="26">26</option>
               <option value="27">27</option>
               <option value="28">28</option>
               <option value="29">29</option>
               <option value="30">30</option>
               <option value="31">31</option>
            </select>
            <select name="wiferes_Year">
               <option value="">Select Year</option>
               <option value="1918">1918</option>
               <option value="1919">1919</option>
               <option value="1920">1920</option>
               <option value="1921">1921</option>
               <option value="1922">1922</option>
               <option value="1923">1923</option>
               <option value="1924">1924</option>
               <option value="1925">1925</option>
               <option value="1926">1926</option>
               <option value="1927">1927</option>
               <option value="1928">1928</option>
               <option value="1929">1929</option>
               <option value="1930">1930</option>
               <option value="1931">1931</option>
               <option value="1932">1932</option>
               <option value="1933">1933</option>
               <option value="1934">1934</option>
               <option value="1935">1935</option>
               <option value="1936">1936</option>
               <option value="1937">1937</option>
               <option value="1938">1938</option>
               <option value="1939">1939</option>
               <option value="1940">1940</option>
               <option value="1941">1941</option>
               <option value="1942">1942</option>
               <option value="1943">1943</option>
               <option value="1944">1944</option>
               <option value="1945">1945</option>
               <option value="1946">1946</option>
               <option value="1947">1947</option>
               <option value="1948">1948</option>
               <option value="1949">1949</option>
               <option value="1950">1950</option>
               <option value="1951">1951</option>
               <option value="1952">1952</option>
               <option value="1953">1953</option>
               <option value="1954">1954</option>
               <option value="1955">1955</option>
               <option value="1956">1956</option>
               <option value="1957">1957</option>
               <option value="1958">1958</option>
               <option value="1959">1959</option>
               <option value="1960">1960</option>
               <option value="1961">1961</option>
               <option value="1962">1962</option>
               <option value="1963">1963</option>
               <option value="1964">1964</option>
               <option value="1965">1965</option>
               <option value="1966">1966</option>
               <option value="1967">1967</option>
               <option value="1968">1968</option>
               <option value="1969">1969</option>
               <option value="1970">1970</option>
               <option value="1971">1971</option>
               <option value="1972">1972</option>
               <option value="1973">1973</option>
               <option value="1974">1974</option>
               <option value="1975">1975</option>
               <option value="1976">1976</option>
               <option value="1977">1977</option>
               <option value="1978">1978</option>
               <option value="1979">1979</option>
               <option value="1980">1980</option>
               <option value="1981">1981</option>
               <option value="1982">1982</option>
               <option value="1983">1983</option>
               <option value="1984">1984</option>
               <option value="1985">1985</option>
               <option value="1986">1986</option>
               <option value="1987">1987</option>
               <option value="1988">1988</option>
               <option value="1989">1989</option>
               <option value="1990">1990</option>
               <option value="1991">1991</option>
               <option value="1992">1992</option>
               <option value="1993">1993</option>
               <option value="1994">1994</option>
               <option value="1995">1995</option>
               <option value="1996">1996</option>
               <option value="1997">1997</option>
               <option value="1998">1998</option>
               <option value="1999">1999</option>
               <option value="2000">2000</option>
               <option value="2001">2001</option>
            </select>
         </div>
      </div>
	   <div>
         <label for="wife_coun_res">County in which wife Resides:</label>
         <input type="text" id="wife_coun_res" name="wife_coun_res">
      </div>
  </div>
      <div>
         <label for="wife_street">Street:</label>
         <input type="text" id="wife_street" name="wife_street">
      </div>
      <div>
         <label for="wife_city">City:</label>
         <input type="text" id="wife_city" name="wife_city">
      </div>
      <div>
         <label for="wife_zip">Zip Code:</label>
         <input type="text" id="wife_zip" name="wife_zip">
      </div>
      <div>
         <label for="wife_county">County:</label>
         <input type="text" id="wife_county" name="wife_county">
      </div>
      <div>
         <label for="wife_curr_res">Address in which Wife currently resides:</label>
         <input type="text" id="wife_curr_res" name="wife_curr_res">
      </div>
      <h4> 
         Mailing Address
      </h4>
      <div>
         <label for="wmail_street">Street:</label>
         <input type="text" id="wmail_street" name="wmail_street">
      </div>
      <div>
         <label for="wmail_city">City:</label>
         <input type="text" id="wmail_city" name="wmail_city">
      </div>
      <div>
         <label for="wmail_zip">Zip Code:</label>
         <input type="text" id="wmail_zip" name="wmail_zip">
      </div>
      <div>
         <label for="wmail_county">County:</label>
         <input type="text" id="wmail_county" name="wmail_county">
      </div>
      <div>
         <label for="wmail_curr_res">Address in which Wife mail received:</label>
         <input type="text" id="wmail_curr_res" name="wmail_curr_res">
      </div>
   </div>
   <h4>
      Wife's Phone
   </h4>
   <div>
      <label for="phone_ans">Answer Block:</label>
      <input type="text" id="phone_ans" name="phone_ans">
   </div>
   <div>
      <label for="phone_home">Home or Cell:</label>
      <input type="text" id="phone_home" name="phone_home">
   </div>
   <h4>
      Date of Birth (The month, day and year Wife was born)
   </h4>
   <div>
      <div id="month" class="date_of_birth">         
            <select class="calculate percentage ignore" id="dor_month" name="dor_month" required1>
               <option value=""> Month </option>
               <option value="1">January</option>
               <option value="2">February</option>
               <option value="3">March</option>
               <option value="4">April</option>
               <option value="5">May</option>
               <option value="6">June</option>
               <option value="7">July</option>
               <option value="8">August</option>
               <option value="9">September</option>
               <option value="10">October</option>
               <option value="11">November</option>
               <option value="12">December</option>
            </select>         
      </div>
      <div id="date"  class="date_of_birth">
         <label class="field select">
            <select class="calculate" id="dob_date" name="dob_date" required1>
               <option value="">Date</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               <option value="16">16</option>
               <option value="17">17</option>
               <option value="18">18</option>
               <option value="19">19</option>
               <option value="20">20</option>
               <option value="21">21</option>
               <option value="22">22</option>
               <option value="23">23</option>
               <option value="24">24</option>
               <option value="25">25</option>
               <option value="26">26</option>
               <option value="27">27</option>
               <option value="28">28</option>
               <option value="29">29</option>
               <option value="30">30</option>
               <option value="31">31</option>
            </select>        
      </div>
      <div id="year" class="date_of_birth">
            <select class="calculate percentage" id="dob_year" name="dob_year" required1>
               <option value=""> Year </option>
               <option value="2018">2018</option>
               <option value="2017">2017</option>
               <option value="2016">2016</option>
               <option value="2015">2015</option>
               <option value="2014">2014</option>
               <option value="2013">2013</option>
               <option value="2012">2012</option>
               <option value="2011">2011</option>
               <option value="2010">2010</option>
               <option value="2009">2009</option>
               <option value="2008">2008</option>
            </select>
      </div>
      <h4>
         Social Security Number
      </h4>
      <div>
         <label for="sos_num">Social security number</label>
         <input type="text" id="sos_num" name="sos_num">
      </div>
      <h4>
         Drive License Number
      </h4>
      <div>
         <label for="state_license_no">
         The State and License number which appears on your license, if none, State N/A
         </label>
         <input type="text" id="state_license_no" name="state_license_no">
      </div>
      <div>
         <label for="gender">Wife's Employer</label>
         <input type="radio" id="wife_emp_yes" name="wife_emp" value="yes"> Yes<br>
         <input type="radio" id="wife_emp_no" name="wife_emp" value="no"> No<br>
      </div>
      <div id="emp_wife_fields" class="hide_this">
		  <div >
			 <label for="emp_address">Employer Address</label>
			 <input type="text" id="emp_address" name="emp_address">
		  </div>
		  <div>
			 <label for="emp_phone">Employer Phone</label>
			 <input type="text" id="emp_phone" name="emp_phone">
		  </div>
		  <div>
         <label for="wife_income">Wife Gross Monthly Income</label>
         <input type="text" id="wife_income" name="wife_income">
      </div>
	  </div>
      
      <div id="wife_ethnicity11">
         <label class="field select">Wife Ethnicity</label>
         <select class="calculate percentage" id="wife_ethnicity" name="wife_ethnicity" required1>
            <option value=""> -- Select -- </option>
            <option value="white">White (Not Hispanic)</option>
            <option value="african">African-American(Hispanic)</option>
            <option value="asian">Asian or Pacific Islander</option>
            <option value="native_american">Native American/Alaskan Native</option>
         </select>
      </div>
   
<div class="row">
<h2>Husband INFORMATION </h2>
   <div class="col">
      <div class="">
         <div class="question">
            <label>Husband's Full Name?</label>
            <input type="text" name="husband_name" size="30" value="">
         </div>
      </div>
      <p>Full legal name. EasyNVDDivorce</p>
      <div class="">
         <h4> 
            Husband's Address
         </h4>
         <div>
            <label for="husband_street">Street:</label>
            <input type="text" id="husband_street" name="husband_street">
         </div>
         <div>
            <label for="husband_city">City:</label>
            <input type="text" id="husband_city" name="husband_city">
         </div>
         <div>
            <label for="husband_zip">Zip Code:</label>
            <input type="text" id="husband_zip" name="husband_zip">
         </div>
         <div>
            <label for="husband_county">County:</label>
            <input type="text" id="husband_county" name="husband_county">
         </div>
	 <label for="husband_nv">Is husband is the resident of Nevada ?</label>
	  <input type="checkbox" id="husband_resident_nev" name="husband_coun_res">
	<div id="show_husband_resident_nev">
		<div>
         <label for="husband_res_date1">Date husband has been a resident:</label>
         <div id="husband_res_date1" class="date_of_birth">
            <select name="husbandres_Month">
               <option value="">Select Month</option>
               <option value="January">January</option>
               <option value="February">February</option>
               <option value="March">March</option>
               <option value="April">April</option>
               <option value="May">May</option>
               <option value="June">June</option>
               <option value="July">July</option>
               <option value="August">August</option>
               <option value="September">September</option>
               <option value="October">October</option>
               <option value="November">November</option>
               <option value="December">December</option>
            </select>
		</div>	
        <div id="hus_res_day" class="date_of_birth">
			<select name="husbandres_Day">
               <option value="">Select Day</option>
               <option value="1">1</option>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
               <option value="13">13</option>
               <option value="14">14</option>
               <option value="15">15</option>
               <option value="16">16</option>
               <option value="17">17</option>
               <option value="18">18</option>
               <option value="19">19</option>
               <option value="20">20</option>
               <option value="21">21</option>
               <option value="22">22</option>
               <option value="23">23</option>
               <option value="24">24</option>
               <option value="25">25</option>
               <option value="26">26</option>
               <option value="27">27</option>
               <option value="28">28</option>
               <option value="29">29</option>
               <option value="30">30</option>
               <option value="31">31</option>
            </select>
		</div>	
        <div id="hus_res_year" class="date_of_birth">
			<select name="husbandres_Year">
               <option value="">Select Year</option>
               <option value="1918">1918</option>
               <option value="1919">1919</option>
               <option value="1920">1920</option>
               <option value="1921">1921</option>
               <option value="1922">1922</option>
               <option value="1923">1923</option>
               <option value="1924">1924</option>
               <option value="1925">1925</option>
               <option value="1926">1926</option>
               <option value="1927">1927</option>
               <option value="1928">1928</option>
               <option value="1929">1929</option>
               <option value="1930">1930</option>
               <option value="1931">1931</option>
               <option value="1932">1932</option>
               <option value="1933">1933</option>
               <option value="1934">1934</option>
               <option value="1935">1935</option>
               <option value="1936">1936</option>
               <option value="1937">1937</option>
               <option value="1938">1938</option>
               <option value="1939">1939</option>
               <option value="1940">1940</option>
               <option value="1941">1941</option>
               <option value="1942">1942</option>
               <option value="1943">1943</option>
               <option value="1944">1944</option>
               <option value="1945">1945</option>
               <option value="1946">1946</option>
               <option value="1947">1947</option>
               <option value="1948">1948</option>
               <option value="1949">1949</option>
               <option value="1950">1950</option>
               <option value="1951">1951</option>
               <option value="1952">1952</option>
               <option value="1953">1953</option>
               <option value="1954">1954</option>
               <option value="1955">1955</option>
               <option value="1956">1956</option>
               <option value="1957">1957</option>
               <option value="1958">1958</option>
               <option value="1959">1959</option>
               <option value="1960">1960</option>
               <option value="1961">1961</option>
               <option value="1962">1962</option>
               <option value="1963">1963</option>
               <option value="1964">1964</option>
               <option value="1965">1965</option>
               <option value="1966">1966</option>
               <option value="1967">1967</option>
               <option value="1968">1968</option>
               <option value="1969">1969</option>
               <option value="1970">1970</option>
               <option value="1971">1971</option>
               <option value="1972">1972</option>
               <option value="1973">1973</option>
               <option value="1974">1974</option>
               <option value="1975">1975</option>
               <option value="1976">1976</option>
               <option value="1977">1977</option>
               <option value="1978">1978</option>
               <option value="1979">1979</option>
               <option value="1980">1980</option>
               <option value="1981">1981</option>
               <option value="1982">1982</option>
               <option value="1983">1983</option>
               <option value="1984">1984</option>
               <option value="1985">1985</option>
               <option value="1986">1986</option>
               <option value="1987">1987</option>
               <option value="1988">1988</option>
               <option value="1989">1989</option>
               <option value="1990">1990</option>
               <option value="1991">1991</option>
               <option value="1992">1992</option>
               <option value="1993">1993</option>
               <option value="1994">1994</option>
               <option value="1995">1995</option>
               <option value="1996">1996</option>
               <option value="1997">1997</option>
               <option value="1998">1998</option>
               <option value="1999">1999</option>
               <option value="2000">2000</option>
               <option value="2001">2001</option>
            </select>
         </div>
      </div>
	   <div>
         <label for="husband_coun_res">County in which husband Resides:</label>
         <input type="text" id="husband_coun_res" name="husband_coun_res">
      </div>
	</div>
         <div>
            <label for="husband_curr_res">Address in which Husband currently resides:</label>
            <input type="text" id="husband_curr_res" name="husband_curr_res">
         </div>
         <h4> 
            Mailing Address
         </h4>
         <div>
            <label for="wmail_street">Street:</label>
            <input type="text" id="wmail_street" name="wmail_street">
         </div>
         <div>
            <label for="wmail_city">City:</label>
            <input type="text" id="wmail_city" name="wmail_city">
         </div>
         <div>
            <label for="wmail_zip">Zip Code:</label>
            <input type="text" id="wmail_zip" name="wmail_zip">
         </div>
         <div>
            <label for="wmail_county">County:</label>
            <input type="text" id="wmail_county" name="wmail_county">
         </div>
         <div>
            <label for="wmail_curr_res">Address in which Husband mail received:</label>
            <input type="text" id="wmail_curr_res" name="wmail_curr_res">
         </div>
      </div>
      <h4>
         Husband's Phone
      </h4>
      <div>
         <label for="phone_ans">Answer Block:</label>
         <input type="text" id="phone_ans" name="phone_ans">
      </div>
      <div>
         <label for="phone_home">Home or Cell:</label>
         <input type="text" id="phone_home" name="phone_home">
      </div>
      <h4>
         Date of Birth (The month, day and year Husband was born)
      </h4>
      <div>
         <div id="month" class="date_of_birth">
               <select class="calculate percentage ignore" id="dor_month" name="dor_month" required1>
                  <option value=""> Month </option>
                  <option value="1">January</option>
                  <option value="2">February</option>
                  <option value="3">March</option>
                  <option value="4">April</option>
                  <option value="5">May</option>
                  <option value="6">June</option>
                  <option value="7">July</option>
                  <option value="8">August</option>
                  <option value="9">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
               </select>
         </div>
         <div id="phone1" class="date_of_birth">
               <select class="calculate" id="dob_date" name="dob_date" required1>
                  <option value="">Date</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                  <option value="24">24</option>
                  <option value="25">25</option>
                  <option value="26">26</option>
                  <option value="27">27</option>
                  <option value="28">28</option>
                  <option value="29">29</option>
                  <option value="30">30</option>
                  <option value="31">31</option>
               </select>
         </div>
         <div id="phone1" class="date_of_birth">
               <select class="calculate percentage" id="dob_year" name="dob_year" required1>
                  <option value=""> Year </option>
                  <option value="2018">2018</option>
                  <option value="2017">2017</option>
                  <option value="2016">2016</option>
                  <option value="2015">2015</option>
                  <option value="2014">2014</option>
                  <option value="2013">2013</option>
                  <option value="2012">2012</option>
                  <option value="2011">2011</option>
                  <option value="2010">2010</option>
                  <option value="2009">2009</option>
                  <option value="2008">2008</option>
               </select>
         </div>
         <h4>
            Social Security Number
         </h4>
         <div>
            <label for="sos_num">Social security number</label>
            <input type="text" id="sos_num" name="sos_num">
         </div>
         <h4>
            Drive License Number
         </h4>
         <div>
            <label for="state_license_no">
            The State and License number which appears on your license, if none, State N/A
            </label>
            <input type="text" id="state_license_no" name="state_license_no">
         </div>
         <div>
            <label for="gender">Husband's Employer</label>
            <input type="radio" id="husband_emp_yes" name="husband_emp" value="yes"> Yes<br>
            <input type="radio" id="husband_emp_no" name="husband_emp" value="no"> No<br>
         </div>
         <div id="emp_husband_feilds" class="hide_this">
         <div>
            <label for="emp_address">Employer Address</label>
            <input type="text" id="emp_address" name="emp_address">
         </div>
         <div>
            <label for="emp_phone">Employer Phone</label>
            <input type="text" id="emp_phone" name="emp_phone">
         </div>
         <div>
            <label for="husband_income">Husband Gross Monthly Income</label>
            <input type="text" id="husband_income" name="husband_income">
         </div>
         </div>
         <div id="husband_ethnicity11">
            <label class="field select">Husband Ethnicity</label>
            <select class="calculate percentage" id="husband_ethnicity" name="husband_ethnicity" required1>
               <option value=""> -- Select -- </option>
               <option value="white">White (Not Hispanic)</option>
               <option value="african">African-American(Hispanic)</option>
               <option value="asian">Asian or Pacific Islander</option>
               <option value="native_american">Native American/Alaskan Native</option>
            </select>
         </div>
      </div>
   </div>
</div>

				<div class="row">
					<div class="col">
						<label for="in_usa">Where were the Husband and Wife married?</label>
						<div>
							<input type="radio" name="were_huswife_married" value="in_usa" id="in_usa" checked> In the United States<br>
							<input type="radio" name="were_huswife_married" value="out_usa" id="out_usa"> Outside the United States<br>
						</div>
					</div>
				</div>
			<div id="in_side_usa">
				<br>
				<label for="marriage_state">Marriage State?</label>
				<select id="marriage_state" name="">
				<option value="">SELECT STATE</option>
				<option value="alabama">Alabama</option>
				<option value="alaska">Alaska</option>
				<option value="arizona">Arizona</option>
				<option value="arkansas">Arkansas</option>
				<option value="california">California</option>
				<option value="colorado">Colorado</option>
				<option value="connecticut">Connecticut</option>
				<option value="delaware">Delaware</option>
				<option value="dc">Wash. DC</option>
				<option value="florida">Florida</option>
				<option value="georgia">Georgia</option>
				<option value="hawaii">Hawaii</option>
				<option value="idaho">Idaho</option>
				<option value="illinois">Illinois</option>
				<option value="indiana">Indiana</option>
				<option value="iowa">Iowa</option>
				<option value="kansas">Kansas</option>
				<option value="kentucky">Kentucky</option>
				<option value="louisiana">Louisiana</option>
				<option value="massachusetts">Massachusetts</option>
				<option value="maryland">Maryland</option>
				<option value="maine">Maine</option>
				<option value="michigan">Michigan</option>
				<option value="minnesota">Minnesota</option>
				<option value="mississippi">Mississippi</option>
				<option value="missouri">Missouri</option>
				<option value="montana">Montana</option>
				<option value="nebraska">Nebraska</option>
				<option value="nevada">Nevada</option>
				<option value="newhampshire">New Hampshire</option>
				<option value="newjersey">New Jersey</option>
				<option value="newmexico">New Mexico</option>
				<option value="newyork">New York</option>
				<option value="northcarolina">North Carolina</option>
				<option value="northdakota">North Dakota</option>
				<option value="ohio">Ohio</option>
				<option value="oklahoma">Oklahoma</option>
				<option value="oregon">Oregon</option>
				<option value="pennsylvania">Pennsylvania</option>
				<option value="rhodeisland">Rhode Island</option>
				<option value="southcarolina">South Carolina</option>
				<option value="southdakota">South Dakota</option>
				<option value="tennessee">Tennessee</option>
				<option value="texas">Texas</option>
				<option value="utah">Utah</option>
				<option value="vermont">Vermont</option>
				<option value="virginia">Virginia</option>
				<option value="washington">Washington</option>
				<option value="westvirginia">West Virginia</option>
				<option value="wisconsin">Wisconsin</option>
				<option value="wyoming">Wyoming</option>
				</select>
				<br>
				<div>
					<div>
						<label for="marriage_city">Marriage City(town, Village,etc)?</label>
						<input type="text" id="marriage_city" name="marriage_city">
					</div>
				</div>
				
				
				<div>
				<div id="answerControls">
					<label for="marriage_Month">Marriage Date?</label>
					<select name="marriage_Month" id="marriage_Month">
						<option value="">Select Month</option><option value="January">January
					</option><option value="February">February
					</option><option value="March">March
					</option><option value="April">April
					</option><option value="May">May
					</option><option value="June">June
					</option><option value="July">July
					</option><option value="August">August
					</option><option value="September">September
					</option><option value="October">October
					</option><option value="November">November
					</option><option value="December">December
					</option></select>
					<select name="marriage_Day" id="marriage_Day">
						<option value="">Select Day</option><option value="1">1
					</option><option value="2">2
					</option><option value="3">3
					</option><option value="4">4
					</option><option value="5">5
					</option><option value="6">6
					</option><option value="7">7
					</option><option value="8">8
					</option><option value="9">9
					</option><option value="10">10
					</option><option value="11">11
					</option><option value="12">12
					</option><option value="13">13
					</option><option value="14">14
					</option><option value="15">15
					</option><option value="16">16
					</option><option value="17">17
					</option><option value="18">18
					</option><option value="19">19
					</option><option value="20">20
					</option><option value="21">21
					</option><option value="22">22
					</option><option value="23">23
					</option><option value="24">24
					</option><option value="25">25
					</option><option value="26">26
					</option><option value="27">27
					</option><option value="28">28
					</option><option value="29">29
					</option><option value="30">30
					</option><option value="31">31
					</option></select>
					<select name="marriage_Year" id="marriage_Year">
						<option value="">Select Year</option><option value="1918">1918
					</option><option value="1919">1919
					</option><option value="1920">1920
					</option><option value="1921">1921
					</option><option value="1922">1922
					</option><option value="1923">1923
					</option><option value="1924">1924
					</option><option value="1925">1925
					</option><option value="1926">1926
					</option><option value="1927">1927
					</option><option value="1928">1928
					</option><option value="1929">1929
					</option><option value="1930">1930
					</option><option value="1931">1931
					</option><option value="1932">1932
					</option><option value="1933">1933
					</option><option value="1934">1934
					</option><option value="1935">1935
					</option><option value="1936">1936
					</option><option value="1937">1937
					</option><option value="1938">1938
					</option><option value="1939">1939
					</option><option value="1940">1940
					</option><option value="1941">1941
					</option><option value="1942">1942
					</option><option value="1943">1943
					</option><option value="1944">1944
					</option><option value="1945">1945
					</option><option value="1946">1946
					</option><option value="1947">1947
					</option><option value="1948">1948
					</option><option value="1949">1949
					</option><option value="1950">1950
					</option><option value="1951">1951
					</option><option value="1952">1952
					</option><option value="1953">1953
					</option><option value="1954">1954
					</option><option value="1955">1955
					</option><option value="1956">1956
					</option><option value="1957">1957
					</option><option value="1958">1958
					</option><option value="1959">1959
					</option><option value="1960">1960
					</option><option value="1961">1961
					</option><option value="1962">1962
					</option><option value="1963">1963
					</option><option value="1964">1964
					</option><option value="1965">1965
					</option><option value="1966">1966
					</option><option value="1967">1967
					</option><option value="1968">1968
					</option><option value="1969">1969
					</option><option value="1970">1970
					</option><option value="1971">1971
					</option><option value="1972">1972
					</option><option value="1973">1973
					</option><option value="1974">1974
					</option><option value="1975">1975
					</option><option value="1976">1976
					</option><option value="1977">1977
					</option><option value="1978">1978
					</option><option value="1979">1979
					</option><option value="1980">1980
					</option><option value="1981">1981
					</option><option value="1982">1982
					</option><option value="1983">1983
					</option><option value="1984">1984
					</option><option value="1985">1985
					</option><option value="1986">1986
					</option><option value="1987">1987
					</option><option value="1988">1988
					</option><option value="1989">1989
					</option><option value="1990">1990
					</option><option value="1991">1991
					</option><option value="1992">1992
					</option><option value="1993">1993
					</option><option value="1994">1994
					</option><option value="1995">1995
					</option><option value="1996">1996
					</option><option value="1997">1997
					</option><option value="1998">1998
					</option><option value="1999">1999
					</option><option value="2000">2000
					</option><option value="2001">2001
					</option></select>
					</div>
				</div>
				
				<div class="row">
					<div class="col">
					<label for="marriage_city1">Marriage City(town, Village,etc)?</label>
					<input type="text" id="marriage_city" name="marriage_city">
				
					</div>
				</div>
			</div>
		<div id="out_side_usa" class="hide_this">
				<div>
						<label for="marriage_country">Marriage Country?</label>
						<input type="text" id="marriage_country" name="marriage_country">
				</div>
<div class="row">
					<div class="col">
					<label for="marriage_city1">Marriage City(town, Village,etc)?</label>
					<input type="text" id="marriage_city" name="marriage_city">
				
					</div>
				</div>				
					<div id="answerControls1">
					<label for="marriage_Month1">Marriage Date?</label>
					<select name="marriage_month_out" id="marriage_month_out">
						<option value="">Select Month</option><option value="January">January
					</option><option value="February">February
					</option><option value="March">March
					</option><option value="April">April
					</option><option value="May">May
					</option><option value="June">June
					</option><option value="July">July
					</option><option value="August">August
					</option><option value="September">September
					</option><option value="October">October
					</option><option value="November">November
					</option><option value="December">December
					</option></select>
					<select name="marriage_day_out">
						<option value="">Select Day</option><option value="1">1
					</option><option value="2">2
					</option><option value="3">3
					</option><option value="4">4
					</option><option value="5">5
					</option><option value="6">6
					</option><option value="7">7
					</option><option value="8">8
					</option><option value="9">9
					</option><option value="10">10
					</option><option value="11">11
					</option><option value="12">12
					</option><option value="13">13
					</option><option value="14">14
					</option><option value="15">15
					</option><option value="16">16
					</option><option value="17">17
					</option><option value="18">18
					</option><option value="19">19
					</option><option value="20">20
					</option><option value="21">21
					</option><option value="22">22
					</option><option value="23">23
					</option><option value="24">24
					</option><option value="25">25
					</option><option value="26">26
					</option><option value="27">27
					</option><option value="28">28
					</option><option value="29">29
					</option><option value="30">30
					</option><option value="31">31
					</option></select>
					<select name="marriage_year_out">
						<option value="">Select Year</option><option value="1918">1918
					</option><option value="1919">1919
					</option><option value="1920">1920
					</option><option value="1921">1921
					</option><option value="1922">1922
					</option><option value="1923">1923
					</option><option value="1924">1924
					</option><option value="1925">1925
					</option><option value="1926">1926
					</option><option value="1927">1927
					</option><option value="1928">1928
					</option><option value="1929">1929
					</option><option value="1930">1930
					</option><option value="1931">1931
					</option><option value="1932">1932
					</option><option value="1933">1933
					</option><option value="1934">1934
					</option><option value="1935">1935
					</option><option value="1936">1936
					</option><option value="1937">1937
					</option><option value="1938">1938
					</option><option value="1939">1939
					</option><option value="1940">1940
					</option><option value="1941">1941
					</option><option value="1942">1942
					</option><option value="1943">1943
					</option><option value="1944">1944
					</option><option value="1945">1945
					</option><option value="1946">1946
					</option><option value="1947">1947
					</option><option value="1948">1948
					</option><option value="1949">1949
					</option><option value="1950">1950
					</option><option value="1951">1951
					</option><option value="1952">1952
					</option><option value="1953">1953
					</option><option value="1954">1954
					</option><option value="1955">1955
					</option><option value="1956">1956
					</option><option value="1957">1957
					</option><option value="1958">1958
					</option><option value="1959">1959
					</option><option value="1960">1960
					</option><option value="1961">1961
					</option><option value="1962">1962
					</option><option value="1963">1963
					</option><option value="1964">1964
					</option><option value="1965">1965
					</option><option value="1966">1966
					</option><option value="1967">1967
					</option><option value="1968">1968
					</option><option value="1969">1969
					</option><option value="1970">1970
					</option><option value="1971">1971
					</option><option value="1972">1972
					</option><option value="1973">1973
					</option><option value="1974">1974
					</option><option value="1975">1975
					</option><option value="1976">1976
					</option><option value="1977">1977
					</option><option value="1978">1978
					</option><option value="1979">1979
					</option><option value="1980">1980
					</option><option value="1981">1981
					</option><option value="1982">1982
					</option><option value="1983">1983
					</option><option value="1984">1984
					</option><option value="1985">1985
					</option><option value="1986">1986
					</option><option value="1987">1987
					</option><option value="1988">1988
					</option><option value="1989">1989
					</option><option value="1990">1990
					</option><option value="1991">1991
					</option><option value="1992">1992
					</option><option value="1993">1993
					</option><option value="1994">1994
					</option><option value="1995">1995
					</option><option value="1996">1996
					</option><option value="1997">1997
					</option><option value="1998">1998
					</option><option value="1999">1999
					</option><option value="2000">2000
					</option><option value="2001">2001
					</option></select>
					</div>
			</div>
			
					<div>
						<label for="spouse_curr">Does each spouse currently posses or have control over debt that they shall be responsible for?</label>
						<input type="radio" id="spouse_curr_yes_check" name="spouse_curr" value="yes"> Yes<br>
						<input type="radio" id="spouse_curr_no_check" name="spouse_curr" value="no"> No<br>
					</div>
					<div id="spouse_curr_yes" class="hide">
						<p>
							All community debts have been previously divided and each is to keep those debts assigned to them and indemnify and hold the other party harmless from those debts.
						</p>
					</div>
					<div id="spouse_curr_no" class="hide">
						<h4>Wife's Marital Debt Obligation</h4>
						<p>
							The portion of martial debt that was acquired during the marriage that the Wife will be obligated to pay. You may list debt such as, credit cards, bank loans, mortgages, car loans, etc. If Wife is responsible for half of the debt and Husband the other half, specify with 50% of the debt.
						</p>
						<div>
							<label for="cap_onecred">Capital One Credit Card, Account No.</label>
							<input type="text" id="cap_onecred" name="cap_onecred">
						</div>
						<div>
							<label for="wife_cont_autoloan">Wife Shall Continue to pay on the auto load for: </label>
							<input type="text" id="wife_cont_autoloan" name="wife_cont_autoloan">
						</div>
						<div>
							<label for="visa_card_acno">Visa Credit Card Account No.: </label>
							<input type="text" id="visa_card_acno" name="visa_card_acno">
						</div>
						<div>
							<label for="wife_cont_autoloan">: </label>
							<input type="text" id="wife_cont_autoloan" name="wife_cont_autoloan">
						</div>
						<div>	
							<label for="wife_debt">Type in Wife's debt :</label>
							<textarea name="wife_debt" id="wife_debt" cols="30" rows="10"></textarea>
						</div>
						<h3>Will appear as:</h3>
						<p>
							The parties agree that the Wife, Shall pay and indemnify and hold the husband harmless from the following marital debt:
						</p>
						<h4>Husband's Marital Debt Obligation</h4>
						<p>
							The portion of martial debt that was acquired during the marriage that the Husband will be obligated to pay. You may list debt such as, credit cards, bank loans, mortgages, car loans, etc. If Husband is responsible for half of the debt and Husband the other half, specify with 50% of the debt.
						</p>
						<div>
							<label for="cap_onecred">Capital One Credit Card, Account No.</label>
							<input type="text" id="cap_onecred" name="cap_onecred">
						</div>
						<div>
							<label for="husband_cont_autoloan">Husband Shall Continue to pay on the auto load for: </label>
							<input type="text" id="husband_cont_autoloan" name="husband_cont_autoloan">
						</div>
						<div>
							<label for="visa_card_acno">Visa Credit Card Account No.: </label>
							<input type="text" id="visa_card_acno" name="visa_card_acno">
						</div>
						<div>
							<label for="husband_cont_autoloan">: </label>
							<input type="text" id="husband_cont_autoloan" name="husband_cont_autoloan">
						</div>
						<div>	
							<label for="husband_debt">Type in Husband's debt :</label>
							<textarea name="husband_debt" id="husband_debt" cols="30" rows="10"></textarea>
						</div>
						<h3>Will appear as:</h3>
						<p>
							The parties agree that the Husband, Shall pay and indemnify and hold the husband harmless from the following marital debt:
						</p>
					</div>
					<div class="row">
							<div class="col">
								<div>
									<label for="marital_home">Is there a Martial Home?</label>
									<input type="radio" id="marital_home_yes" name="marital_home" value="yes"> Yes<br>
									<input type="radio" id="marital_home_no" name="marital_home" value="no"> No<br>
								</div>
								<div id="show_marital_home_adress" class="hide_this">
									<label for="mart_hm_address">Marital Home Address</label>
									<input type="text" id="mart_hm_address" name="mart_hm_address">
									<h3>What is going to happen to the marital home?</h3>
									<input type="radio" id="marital_home_transferred" name="marital_home_action" value="transferred">The marital home will be transferred from one spouse to another.</br>
									<input type="radio" id="marital_home_sold" name="marital_home_action" value="sold"> The marital home will be sold and the husband and wife will share the proceeds or debt.
								</div>
							</div>
						</div>

				<div id="home_transferrer" class="hide_this">
					<div>
						<label for="spouse_transferer">Which Spouse will be the transferrer?</label>
						<input type="radio" name="spouse_transferer" value="yes"> Husband<br>
						<input type="radio" name="spouse_transferer" value="no"> Wife<br>
					</div>
					<div>
						<label for="spouse_transferee">Which Spouse will be the transferee?</label>
						<input type="radio" name="spouse_transferee" value="yes"> Husband<br>
						<input type="radio" name="spouse_transferee" value="no"> Wife<br>
					</div>
					
					<div>
						<div>
							<label for="cap_onecred">How much will the transferee pay the transferrer for the marital home?</label>
							$<input type="text" id="transferee_pay" name="transferee_pay">(example format: $50,000.00)
						</div>
						
						<div>
							<label for="visa_card_acno">What is the actual(or approximate) date the transfer of the material home will or has taken place? </label>
							
							<div id="month" class="date_of_birth">
							<select class="calculate percentage ignore" id="vehicle_reg_date1=2" name="dor_month" required1>
							<option value=""> Select Month </option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>										
							</select>
							</div>
							
							<div id="date" class="date_of_birth">
							<select class="calculate percentage ignore" id="vehicle_reg_date1" name="vehicle_reg_date" required1>
							<option value=""> Select Date</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>												
							</select>
							</div>

							<div id="year" class="date_of_birth">
							<select class="calculate percentage" id="vehicle_reg_date3" name="vehicle_reg_date3" required1>
							<option value=""> Select Year </option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>											
							</select>
							</div>
						</div>
						
						<div>
							<label for="wife_cont_autoloan">: </label>
							<input type="text" id="wife_cont_autoloan" name="wife_cont_autoloan">
						</div>
						<div>	
							<label for="wife_debt">Type in Wife's debt :</label>
							<textarea name="wife_debt" id="wife_debt" cols="30" rows="10"></textarea>
						</div>
						<h3>Will apprear as:</h3>
						<p>
							The partise agree that the Wife, Shall pay and indemnify and hold the husband harmless from the following marital debt:
						</p>
						<div>	
							<label for="hus_debt">Type in Husband's debt :</label>
							<textarea name="hus_debt" id="hus_debt" cols="30" rows="10"></textarea>
						</div>
					</div>
				</div>
				<div id="home_sold" class="hide_this">	
					<h2>For if 2</h2>
						<div>	
							<label for="hus_debt">What is the husband's percentage or doller ammount he will receive or be responsible for (in case of a loss) from the sale of the meterial home?</label>
							<input type="text" name="hus_percentage" id="hus_percentage" />% or doller ammount: $<input type="text" name="hus_doller_ammount" id="hus_doller_ammount" />
						</div>
						<div>	
							<label for="hus_debt">What is the wife's percentage or dollar amount she will receive or be responsible for (in case of a loss) from the sale of the meterial home?</label>
							<input type="text" name="wife_percentage" id="wife_percentage" />% or doller ammount: $<input type="text" name="wife_doller_ammount" id="wife_doller_ammount" />
						</div>
						<div>	
							<label for="hus_debt">When will the material home be sold?</label>
							<input type="text" name="mat_home_sold1" id="mat_home_sold1" /> <input type="text" name="mat_home_sold2" id="mat_home_sold2" /> <input type="text" name="mat_home_sold3" id="mat_home_sold3" />
						</div>
					</div>
<div class="row">
			<div class="col">
				<div>
					<h3>Will be entitled to any marital portion of Husband's pension benefit, retirement fund, profit sharing plan or 401K?</h3>
					<input type="radio" id="portion_benefit_yes" name="portion_benefit" value="yes"> Yes<br>
						<input type="radio" id="portion_benefit_no" name="portion_benefit" value="no"> No<br>	
					<label for="portion_benefit">
						Any portion of a benefit, found, profit sharing plan and/or 401k that was earned during the marriage is considered marital property and is potentially up for division.
						<br>
						Keep in mind that it is often suggested that the value of any marital portion of any account be offset with another marital asset. This way the account does not have to be divided.
					</label>
									
							<p id="portion_no">
								The parties each waive all claims, present and future to the other's pension benefits, retirement fund, 401k's profit sharing plans and accounts of the like.</p>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col">
			<p id="portion_yes">
								The parties agree that the marital portion of all present pension benefits, founds, 401k's, profit sharing plans and accounts of the like will be address follows:
							</p>
				<label for="pension_benefit">
					Which spouse will give portion of the pension benefit, retirement fund, profit sharing plan or 401k?
				</label>
				<input type="radio" name="portion_benefit1" value="husband"> Husband<br>
						<input type="radio" name="portion_benefit1" value="wife"> Wife<br>

						<label for="retirement_fund">
					Name of retirement fund | 401k?
				</label>
				<input type="text" name="retirement_fund" value=""> 
				<label for="retirement_fund">
					Acc#
				</label>
				<input type="text" name="account_no" value=""> 
				<br>
				<label for="pension_benefit">
					Which spouse will give portion of the pension benefit, retirement fund, profit sharing plan or 401k?
				</label>
				<input type="radio" name="portion_benefit2" value="husband"> Husband<br>
						<input type="radio" name="portion_benefit2" value="wife"> Wife<br>

			</div>
		</div>	
		<div class="row">
			<div class="col">
				<label for="percentage">What is the percentage or dollar amount of the marital portion the benefit/fund/plan/401k that the spouse will be receiving?</label>
				<input type="text" name="percentage" value="%">
				<label for="">or Dollar Amount</label><input type="text" name="por_dollar" value="">
			</div>
		</div>
		
			<div class="col">
				<label for="percentage">What is the percentage or dollar amount of the marital portion the benefit/fund/plan/401k that the spouse will be receiving?</label>
				<input type="text" name="percentage" value="%">
				<label for="">or Dollar Amount</label><input type="text" name="por_dollar" value="">
			</div>
		
		<div>
						<label for="spouse_transferer">Will either spouse be responsible for maintain health or medical insurance for other spouse?</label>
						<input type="radio" name="spouse_med_resp" value="yes"> Husband<br>
						<input type="radio" name="spouse_med_resp" value="no"> Wife<br>
					</div>
					<div>
						<h3>Answer Yes</h3>
						<p>Yes detail</p>
					</div>
					<div>
						<h3>Answer No</h3>
						<p>No detail</p>
					</div>
					<div>
						<label for="spouse_transferer">Will either Party receive spousal support/maintainence/alimony?</label>
						<input type="radio" name="party_recieve_sp_support" value="yes"> Yes<br>
						<input type="radio" name="party_recieve_sp_support" value="no"> No<br>
					</div>
					<div>
						<h3>Answer Yes</h3>
						<p>Yes detail</p>
						<p>Until date <input type="text" id="until_date11" name="until_date11"></p>
					</div>
					<div>
						<h3>Answer No</h3>
						<p>No detail</p>
					</div>
<div>
						<label for="spouse_transferer">Who will be the resident for the divorce?</label>
						<input type="radio" name="divorce_resident" value="yes"> Husband<br>
						<input type="radio" name="divorce_resident" value="no"> Wife<br>
					</div>
					<div>	
						<label for="name_res_witness">Name of resident Witness</label>
						<input type="text" name="name_res_witness" id="name_res_witness" />
					</div>
					<div>	
						<h3>Expl:</h3>
						<p>The resident witness ...</p>
					</div>
					<div>	
						<label for="who_do_know">Who do they know (Husband/Wife)</label>
						<input type="text" name="who_do_know" id="who_do_know" />
					</div>
					
					<div>
							<label for="visa_card_acno">How long has your resident witness named above lived in Naveda? </label>
							
							<div id="month" class="date_of_birth">
							<select class="calculate percentage ignore" id="how_long_witness_navada1" name="how_long_witness_navada1" required1>
							<option value=""> Select Month </option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>										
							</select>
							</div>
							
							<div id="date" class="date_of_birth">
							<select class="calculate percentage ignore" id="how_long_witness_navada1" name="how_long_witness_navada1" required1>
							<option value=""> Select Date</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>												
							</select>
							</div>

							<div id="year" class="date_of_birth">
							<select class="calculate percentage" id="how_long_witness_navada1" name="how_long_witness_navada1" required1>
							<option value=""> Select Year </option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>											
							</select>
							</div>
						</div>
						
						<div>	
							<label for="who_do_know">Resident Witness Address</label>
							<textarea type="text" name="who_do_know" id="who_do_know"> </textarea>
						</div>
						<div>	
							<h3>Expl:</h3>
							<p>Where your resident witness lives?</p>
							<p>How often does your resident witness see you?</p>
							<input type="text" name="resident_witness" id="resident_witness" />
						</div>
						
					<h2>Children Info</h2>
					<h3>Information for Child 1</h3>
					
					<div>	
						<label for="child_full_name">Full Name</label>
						<input type="text" name="child_full_name" id="child_full_name" />
					</div>
					<div>	
						<label for="child_age">Child_age</label>
						<select class="calculate percentage" id="child_age" name="child_age" required1>
							<option value=""> Select Age</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>												
							</select>
					</div>
					
					<div>
						<label for="spouse_transferer">Sex</label>
						<input type="radio" name="child_sex" value="male"> Male<br>
						<input type="radio" name="child_sex" value="female"> Female<br>
						<input type="radio" name="child_sex" value="unborn"> Unborn<br>
					</div>
					
					<div>	
						<label for="child_ssno">SS#</label>
						<input type="text" name="child_ssno" id="child_ssno" />
					</div>
					
					<div>
							<label for="visa_card_acno">Date of Birth </label>							
							
							<div id="month" class="date_of_birth">
							<select class="calculate percentage" id="child_dob_1" name="child_dob_1" >
							<option value=""> Select Month </option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>										
							</select>
							</div>
							
							<div id="phone1" class="date_of_birth">
							<select class="calculate percentage" id="child_dob_2" name="child_dob_2" >
							<option value=""> Select Date</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>												
							</select>
							</div>

							<div id="phone1" class="date_of_birth">
							<select class="calculate percentage" id="child_dob_3" name="child_dob_3" >
							<option value=""> Select Year </option>
							<option value="2018">2018</option>
							<option value="2017">2017</option>
							<option value="2016">2016</option>
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>											
							</select>
							</div>
						</div>
					
					<div>	
						<label for="child_placebirth">Place of Birth</label>
						<input type="text" name="child_placebirth" id="child_placebirth" />
					</div>
					<h3>Current Address:</h3>
					
					<div>	
						<label for="current_add_street">Street</label>
						<input type="text" name="current_add_street" id="current_add_street" />
					</div>
					<div>	
						<label for="current_add_city">City</label>
						<input type="text" name="current_add_city" id="current_add_city" />
					</div>
					<div>	
						<label for="current_add_state">State/Province</label>
						<input type="text" name="current_add_state" id="current_add_state" />
					</div>
					<div>	
						<label for="current_add_zip">Zip/Postal Code</label>
						<input type="text" name="current_add_zip" id="current_add_zip" />
					</div>
					<div>	
						<label for="current_res_length">Length of Residence</label>
						<input type="text" name="current_res_length" id="current_res_length" />
					</div>
					<div>	
						<label for="current_res_with">Currently Resides with</label>
						<input type="text" name="current_res_with" id="current_res_with" />
					</div>
					
						<h3>Previous Address #1 (Only if within past five years)</h3>
					
					<div>	
						<label for="pre_add_street">Street</label>
						<input type="text" name="pre_add_street" id="pre_add_street" />
					</div>
					<div>	
						<label for="pre_add_city">City</label>
						<input type="text" name="pre_add_city" id="pre_add_city" />
					</div>
					<div>	
						<label for="pre_add_state">State/Province</label>
						<input type="text" name="pre_add_state" id="pre_add_state" />
					</div>
					<div>	
						<label for="pre_add_zip">Zip/Postal Code</label>
						<input type="text" name="pre_add_zip" id="pre_add_zip" />
					</div>
					<div>	
						<label for="pre_res_length">Length of Residence</label>
						<input type="text" name="pre_res_length" id="pre_res_length" />
					</div>
					<div>	
						<label for="pre_res_with">Previously Resides with</label>
						<input type="text" name="pre_res_with" id="pre_res_with" />
					</div>
		<h3>Previous Address #2 (Only if within past five years)</h3>
					
					<div>	
						<label for="pre2_add_street">Street</label>
						<input type="text" name="pre2_add_street" id="pre2_add_street" />
					</div>
					<div>	
						<label for="pre2_add_city">City</label>
						<input type="text" name="pre2_add_city" id="pre2_add_city" />
					</div>
					<div>	
						<label for="pre2_add_state">State/Province</label>
						<input type="text" name="pre2_add_state" id="pre2_add_state" />
					</div>
					<div>	
						<label for="pre2_add_zip">Zip/Postal Code</label>
						<input type="text" name="pre2_add_zip" id="pre2_add_zip" />
					</div>
					<div>	
						<label for="pre2_res_length">Length of Residence</label>
						<input type="text" name="pre2_res_length" id="pre2_res_length" />
					</div>
					<div>	
						<label for="pre2_res_with">Previously Resides with</label>
						<input type="text" name="pre2_res_with" id="pre2_res_with" />
					</div>
					
					
										
					<div>
						<label for="other_courtcase">Has there been any other court case in which you have participated as a party, witness, or in 
					any other way concerning the custody of a visitation with the child(ren)?</label>
						<input type="radio" name="other_courtcase" value="yes"> Yes<br>
						<input type="radio" name="other_courtcase" value="no"> No<br>
					</div>
					
					<div>
						<label for="child_court">Court</label>
						<input type="text" name="child_court" id="child_court" />
					</div>
					<div>
						<label for="child_caseno">Case no</label>
						<input type="text" name="child_caseno" id="child_caseno" />
					</div>
					<div>
						<label for="date_demonstration">Date of Demonstration</label>
						<input type="text" name="date_demonstration" id="date_demonstration" />
					</div>
					<div>
						<label for="additional_children">Are there any additional Children</label>
						<input type="radio" id="additional_children" name="additional_children" value="yes"> Yes<br>
						<input type="radio" id="additional_children" name="additional_children" value="no"> No<br>
					</div>
						<div class="row">
			<div class="col">
			===============
				<label for="pregnant">
					Is wife pregnant at this time? 
				</label>
				<input type="radio" id="wife_pregnant_yes" name="wife_pregnant" value="yes"> Yes<br>
				<input type="radio" id="wife_pregnant_no" name="wife_pregnant" value="no">No<br>
				<div id="is_pregnant">
				<label for="pregnant">
					Is husband the father? 
				</label>
				<input type="radio" id="husband_father_yes" name="husband_father" value="yes"> Yes<br>
				<input type="radio" id="husband_father_no" name="husband_father" value="no">No<br>
				<label for="expected_date">
					Expected date? 
				</label>
				<input type="text" id="expected_date" name="expected_date" value="">
				</div>
			</div>
						===============

				<div id="div_physical_Custody">
				<label class="field select">Who will have the physical Custody of children </label>
				<select class="calculate percentage" id="physical_Custody" name="physical_Custody" >
				<option value=""> Select Year </option>
				<option value="sole_wife">Sole Wife</option>
				<option value="sole_husband">Sole Husband</option>
				<option value="Joint">Joint</option>											
				</select>
				</div>
				<li id="field_18_92" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
         <h2 style="text-align: center;">X.</h2>
         <h3 style="text-align: center;"><strong>HOLIDAY SCHEDULE</strong></h3>
         <p style="text-align: left;"><strong>The holiday schedule shall be as follows:</strong></p>
      </li>
<div id="field_18_93" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_93">Thanksgiving will be altered with Mother/Father having the child(ren) in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_93" id="input_18_93" type="text" value="" class="medium" tabindex="48" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_94" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_94">Christmas Eve will be altered with Mother/Father having the child(ren) in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_94" id="input_18_94" type="text" value="" class="medium" tabindex="49" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_95" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_95">Christmas will be altered with Mother/Father having the child(ren) in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_95" id="input_18_95" type="text" value="" class="medium" tabindex="50" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_96" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_96">Easter will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_96" id="input_18_96" type="text" value="" class="medium" tabindex="51" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_97" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_97">New Year’s Day will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_97" id="input_18_97" type="text" value="" class="medium" tabindex="52" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_98" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_98">Memorial Day will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_98" id="input_18_98" type="text" value="" class="medium" tabindex="53" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_99" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_99">Fourth of July will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_99" id="input_18_99" type="text" value="" class="medium" tabindex="54" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_100" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_100">Labor Day will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_100" id="input_18_100" type="text" value="" class="medium" tabindex="55" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_101" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_101">Nevada Day will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_101" id="input_18_101" type="text" value="" class="medium" tabindex="56" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_102" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_102">Halloween will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_102" id="input_18_102" type="text" value="" class="medium" tabindex="57" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_103" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_103">Veteran’s Day will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_103" id="input_18_103" type="text" value="" class="medium" tabindex="58" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_104" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_104">Child(ren)’s Birthday will be altered with Mother/Father having the child(ren)in the year</label>
         <div class="ginput_container ginput_container_text"><input name="input_104" id="input_18_104" type="text" value="" class="medium" tabindex="59" placehdivder="Years" aria-invadivd="false"></div>
         <div class="gfield_description">and each even/odd year thereafter. </div>
      </div>
      <div id="field_18_105" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible">
         <p style="text-adivgn: left;">Mother shall have the child(ren)on Mother’s Day and Father will have the minor child(ren)on Father’s Day.
            Mother shall have the child(ren)on her birthday and Father shall have the child(ren)on his birthday.
         </p>
      </div>
      <div id="field_18_106" class="gfield gf_indivne gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">Hodivdays not specifically time defined shall begin that</p>
      </div>
      <div id="field_18_107" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_107"></label>
         <div class="ginput_container ginput_container_text"><input name="input_107" id="input_18_107" type="text" value="" class="small" tabindex="60" aria-invadivd="false"></div>
      </div>
      <div id="field_18_108" class="gfield gf_indivne gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">of the hodivday week at </p>
      </div>
      <div id="field_18_109" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_109"></label>
         <div class="ginput_container ginput_container_text"><input name="input_109" id="input_18_109" type="text" value="" class="medium" tabindex="61" aria-invadivd="false"></div>
      </div>
      <div id="field_18_110" class="gfield gf_indivne gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">and end at </p>
      </div>
      <div id="field_18_113" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_113"></label>
         <div class="ginput_container ginput_container_text"><input name="input_113" id="input_18_113" type="text" value="" class="medium" tabindex="62" aria-invadivd="false"></div>
      </div>
      <div id="field_18_112" class="gfield gf_indivne gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">on that  </p>
      </div>
      <div id="field_18_111" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_111"></label>
         <div class="ginput_container ginput_container_text"><input name="input_111" id="input_18_111" type="text" value="" class="medium" tabindex="63" aria-invadivd="false"></div>
      </div>
      <div id="field_18_114" class="gfield gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         Each parent shall have a block time of time with the child(ren)for vacation purposes. That length of time for vacation period shall be: One Week/ Two Weeks
         Father/Mother shall notify the other parent, <u>in writing</u>, at least 2 weeks in advance of the choice of time for the vacation.
         &nbsp;
         Each parent shall notify the other if they take the child(ren)out of the State of Nevada for more than 72 hours, for any reason. Notification shall be made prior to leaving the State and shall include the date leaving the State, the destination, the date returning to the State, the type of transportation, and, if possible, a telephone number for contact while the child(ren)is out of the State. &nbsp;&nbsp;&nbsp; Acquiescence to travel out of state is not required1 by the other parent, only notification of such travel.
         Each parent shall immediately notify the other if an emergency occurs with the child(ren)such that medical treatment is needed or sought.
         Each parent shall/not keep the other informed of the childcare giver for the child(ren), including name, address and telephone number.
         Each parent shall have the right of first refusal to care for the child(ren)if the other parent is not available to care for the child(ren)for a period of 24 hours. In other words, if the child is/are in Mother’s custody and Mother is not available to care for the child for 24 hours or more, Father shall be notified and given the right of first refusal to care for the child(ren)before any third party is called in to care for the child(ren). Mother has the same right of refusal when the child(ren)is/are with Father and Father is not available to care for the child(ren)for 24 hours, or more.
         Both parents are to have equal access to all the child(ren)’s medical records, schodiv records, and any other records generated for the benefit of, or on behalf of, the child(ren).
         <h3 style="text-adivgn: center;"><strong>XI.</strong></h3>
         <h3 style="text-adivgn: center;"><u>CHILD SUPPORT</u></h3>
         Mother/Father shall pay child support in the amount of $000.00 per month, per child, for a total monthly child support obdivgation of $000.00. The child support shall be paid on or before the first day of each month.
         The parties came to this agreement based upon the fdivlowing information:
         Husband’s gross monthly income is $0,000 (amount earned per month before deductions).
         Wife’s gross monthly income is $0,000 (amount earned per month before deductions).
         Father is the non-custodial parent and, the amount agreed upon above is in compdivance with NRS 125B.070 and is 18%/25%/29%/31% of Wife’s/Father’s gross monthly income or the statutory minimum amount.
         OR
         The parents are joint physical custodians, the amount of child support agreed upon meets the statutory requirement.
         OR
         The support obdivgation amount agreed upon by the parties is not the amount required1 in the statutes. Under the statutes, the child support obdivgation for Mother/Father would be $000.00 per month per child. However, that amount should be different based upon NRS 125B.080:
         <ul>
            <div>the cost of health insurance</div>
            <div>the cost of child care</div>
            <div>special educational needs of the child</div>
            <div>the age of the child</div>
            <div>the legal responsibidivty of the parents for the support of others</div>
            <div>the value of services contributed by either parent</div>
            <div>any pubdivc assistance paid to support the child</div>
            <div>any expenses reasonably relate to the mother’s pregnancy and confinement</div>
            <div>the cost of transportation of the child to and from visitation if the custodial parent moved with the child from the jurisdiction of the court which ordered the support and the noncustodial parent remained</div>
            <div>the amount of time the child spends with each parent</div>
            <div>any other necessary expenses for the benefit of the child</div>
            <div>the relative income of both parents</div>
         </ul>
         The child support obdivgation for each child shall continue until that child reaches the age of eighteen years, or, if the child is still attending high schodiv at the age of eighteen years, until the child reaches the age of nineteen years or graduates from high schodiv, or is otherwise emancipated, whichever occurs first.
         A wage assignment for the child support will/not be immediately put in place.
         There is already a Child Support action through the District Attorney’s Office and payment of the child support shall continue to be handled through that office.
         OR
         The children are receiving Welfare benefits and the Welfare Department has, or will have, a child support case through the District Attorney’s Office and the District Attorney’s Office shall continue to handle the child support payments.
         OR
         No formal child support obdivgation has ever previously been estabdivshed and this will be the first Court Order for child support and the parent paying child support will pay the support directly to the receiving parent.
         OR
         &nbsp;
         Although this is the first Court Order for child support, the payments will be through the District Attorney’s Office and the parent who will be cdivlecting child support shall open the case with the District Attorney’s Office.
         <h3 style="text-adivgn: center;"><strong>XII.</strong></h3>
         <h3 style="text-adivgn: center;"><u>HEALTH CARE</u></h3>
         The child(ren)is/not presently covered by health insurance podivcy.
         The child(ren)is presently not on Medicaid.
         Father/Mother shall maintain health insurance on the child through his/her employment.
         The parties shall each share, equally, any health expenses incurred on behalf of the child(ren)that are not covered by insurance, and each party shall be responsible for one half of the deductible and one half of the insurance premium.
         Mother currently pays $000.00 per month for the minor child(ren)’s medical insurance.
         Father shall pay half this expense in the amount of $000.00 per month.
         &nbsp;
         <h3 style="text-adivgn: center;"><u>TAXES</u></h3>
         Mother shall claim the child(ren)on her taxes for 20___ and each even/odd year thereafter.
         Father shall claim the child(ren)on his taxes for 20___ and each even/odd year thereafter.
         OR
         Mother/Father shall claim the child(ren)on his/her taxes each year as she is has primary physical custody of the minor child.
         <h3 style="text-adivgn: center;"><strong>XIII.</strong></h3>
         <h3 style="text-adivgn: center;"><u>ASSETS</u></h3>
         The Petitioners affirmatively state that they have agreed as to the nature of the community property and division thereof of any and all community assets prior to the commencement of this action and each is to keep the property they have in their possession.
         OR
         Additionally, the community property should be divided as fdivlows:
         WIFE shall receive the fdivlowing:
         HUSBAND shall receive the fdivlowing:
         &nbsp;
         <h3 style="text-adivgn: center;"><strong>XIV.</strong></h3>
         <h3 style="text-adivgn: center;"><u>DEBTS</u></h3>
         Petitioners affirmatively state that all of the community debts have been previously divided
         and each is to keep those debts assigned to them and indemnify and hdivd the other party harmless from those debts.
         OR
         Additionally, the community debts should be divided as fdivlows:
         WIFE shall be responsible for the fdivlowing:
         HUSBAND shall be responsible for the fdivlowing:
         Petitioners hereby certify that they have disclosed all community assets and debts and that there are no other community assets or debts for this Court to divide.<strong>.</strong>
      </div>
      <div id="field_18_116" class="gfield gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         <h3 style="text-adivgn: center;"><strong>XV.</strong></h3>
         <h3 style="text-adivgn: center;"><strong>SPOUSAL SUPPORT</strong></h3>
         <p style="text-adivgn: center;">Adivmony and spousal support for either parties is not appropriate in this case.
            OR
         </p>
      </div>
      <div id="field_18_65" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_65"></label>
         <div class="ginput_container ginput_container_text"><input name="input_65" id="input_18_65" type="text" value="" class="medium" tabindex="64" aria-invadivd="false"></div>
         <div class="gfield_description">Name of wife/husband </div>
      </div>
      <div id="field_18_66" class="gfield gf_indivne gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">will receive adivmony in the amount of</p>
      </div>
      <div id="field_18_67" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_67"></label>
         <div class="ginput_container ginput_container_text"><input name="input_67" id="input_18_67" type="text" value="" class="medium" tabindex="65" aria-invadivd="false"></div>
         <div class="gfield_description">$XXX </div>
      </div>
      <div id="field_18_68" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_68"></label>
         <div class="ginput_container ginput_container_text"><input name="input_68" id="input_18_68" type="text" value="" class="medium" tabindex="66" aria-invadivd="false"></div>
         <div class="gfield_description">Per week/month</div>
      </div>
      <div id="field_18_69" class="gfield gf_indivne gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">due on the 1st day of each month for</p>
      </div>
      <div id="field_18_70" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_70"></label>
         <div class="ginput_container ginput_container_text"><input name="input_70" id="input_18_70" type="text" value="" class="medium" tabindex="67" aria-invadivd="false"></div>
         <div class="gfield_description">X months/years</div>
      </div>
      <div id="field_18_71" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible"></div>
      <div id="field_18_72" class="gfield gf_indivne gfield_html gfield_html_formatted gfield_no_fdivlows_desc field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">The adivmony shall begin on:</p>
      </div>
      <div id="field_18_73" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_73"></label>
         <div class="ginput_container ginput_container_text"><input name="input_73" id="input_18_73" type="text" value="" class="medium" tabindex="68" aria-invadivd="false"></div>
         <div class="gfield_description">Date</div>
      </div>
      <div id="field_18_74" class="gfield gf_indivne gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible">
         
         <p style="text-adivgn: left;">and</p>
      </div>
      <div id="field_18_75" class="gfield gf_indivne field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_75"></label>
         <div class="ginput_container ginput_container_text"><input name="input_75" id="input_18_75" type="text" value="" class="medium" tabindex="69" aria-invadivd="false"></div>
         <div class="gfield_description">End Date</div>
      </div>
      <div id="field_18_76" class="gfield gfield_html gfield_html_formatted field_sublabel_below field_description_below gfield_visibidivty_visible">
         <u>FORMER NAME OF WIFE</u>
         Wife wishes to return to her former name of:
         OR
         Wife never changed her name, and therefore, does not request a restoration of her former name.
         <p style="text-adivgn: center;"><strong>XVII.</strong></p>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The parties are incompatible in marriage and there is no hope for reconcidivation, and/or the parties have divved separate and apart for more than one year without cohabitation.&nbsp; It is understood by the Petitioners that entry of Decree of Divorce constitutes a final adjudication of the rights and obdivgations of the parties with respect to the status of the marriage.
         Petitioners each expressly give up their respective rights to receive written Notice of Entry of any Decree and Judgment of Divorce and Petitioners give up their right to request a formal Findings of Fact and Conclusions of Law, or to appeal any Judgment or Order of this Court made and entered in these proceedings or the right to move for a new trial.
         <p style="text-adivgn: center;"><strong>XVIII.</strong></p>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It is further understood by the Petitioners that a final Decree of Divorce entered by this summary procedure does not prejudice of prevent the rights of either Petitioner to bring an action to set aside the final decree for fraud, duress, accident, mistake or the grounds recognized at law or in equity.
         WHEREFORE, Petitioners pray that the court enter an order as fdivlows:
         <div>
            <div>That the bonds of matrimony heretofore and now existing between the Petitioners be</div>
         </div>
         forever dissdivved, and that each of the parties be restored to the status of unmarried persons.
         <div start="2">
            <div>That the terms agreed upon in this Joint Petition be ratified, approved, adopted and</div>
         </div>
         included by the Court and fully merged and incorporated in the Decree.
         <div start="3">
            <div>For other and further redivef as the Court may deem just and proper in this action.</div>
         </div>
         <u>AFFIRMATION</u>
         Pursuant to NRS 239B.030, the undersigned does hereby affirm that the preceding document does not contain the social security number of any person.
         We declare, under penalty of perjury under the law of the State of Nevada, that the foregoing is true and correct.
      </div>
      <div id="field_18_77" class="gfield field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_77">DATED on</label>
         <div class="ginput_container ginput_container_text"><input name="input_77" id="input_18_77" type="text" value="" class="medium" tabindex="70" aria-invadivd="false"></div>
      </div>
      <div id="field_18_78" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_78">Your Name</label>
         <div class="ginput_container ginput_container_text"><input name="input_78" id="input_18_78" type="text" value="" class="medium" tabindex="71" aria-invadivd="false"></div>
      </div>
      <div id="field_18_79" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_79">Spouse’s Name</label>
         <div class="ginput_container ginput_container_text"><input name="input_79" id="input_18_79" type="text" value="" class="medium" tabindex="72" aria-invadivd="false"></div>
      </div>
      <div id="field_18_80" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_80">Address</label>
         <div class="ginput_container ginput_container_text"><input name="input_80" id="input_18_80" type="text" value="" class="medium" tabindex="73" aria-invadivd="false"></div>
      </div>
      <div id="field_18_81" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_81">Address</label>
         <div class="ginput_container ginput_container_text"><input name="input_81" id="input_18_81" type="text" value="" class="medium" tabindex="74" aria-invadivd="false"></div>
      </div>
      <div id="field_18_82" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_82">City, State, Zip</label>
         <div class="ginput_container ginput_container_text"><input name="input_82" id="input_18_82" type="text" value="" class="medium" tabindex="75" aria-invadivd="false"></div>
      </div>
      <div id="field_18_83" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_83">City, State, Zip</label>
         <div class="ginput_container ginput_container_text"><input name="input_83" id="input_18_83" type="text" value="" class="medium" tabindex="76" aria-invadivd="false"></div>
      </div>
      <div id="field_18_84" class="gfield gf_left_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_84">Tel</label>
         <div class="ginput_container ginput_container_phone"><input name="input_84" id="input_18_84" type="text" value="" class="medium" tabindex="77" aria-invadivd="false"></div>
      </div>
      <div id="field_18_85" class="gfield gf_right_half field_sublabel_below field_description_below gfield_visibidivty_visible">
         <label class="gfield_label" for="input_18_85">Tel</label>
         <div class="ginput_container ginput_container_phone"><input name="input_85" id="input_18_85" type="text" value="" class="medium" tabindex="78" aria-invadivd="false"></div>
      </div>
	  
   ====================
					<h2>Visitation Schedule</h2>
					<h3>Visitation Schedule of the minor Child(ren) shall be as follows</h3>
					
					
					<div>	
						<label for="fathr_child_on">Father Shall have the children on</label>
						<input type="checkbox" name="fathr_child_on" value="Monday">Monday<br>
						<input type="checkbox" name="fathr_child_on" value="Tuesday">Tuesday<br>
						<input type="checkbox" name="fathr_child_on" value="Wednesday">Wednesday<br>
						<input type="checkbox" name="fathr_child_on" value="Thursday">Thursday<br>
						<input type="checkbox" name="fathr_child_on" value="Friday">Friday<br>
						<input type="checkbox" name="fathr_child_on" value="Saturday">Saturday<br>
						<input type="checkbox" name="fathr_child_on" value="Sunday">Sunday<br>
					</div>
					<div>	
						<label for="mother_child_on">Mother Shall have the children on</label>
						<input type="checkbox" name="mother_child_on" value="Monday">Monday<br>
						<input type="checkbox" name="mother_child_on" value="Tuesday">Tuesday<br>
						<input type="checkbox" name="mother_child_on" value="Wednesday">Wednesday<br>
						<input type="checkbox" name="mother_child_on" value="Thursday">Thursday<br>
						<input type="checkbox" name="mother_child_on" value="Friday">Friday<br>
						<input type="checkbox" name="mother_child_on" value="Saturday">Saturday<br>
						<input type="checkbox" name="mother_child_on" value="Sunday">Sunday<br>
					</div>
					<div>	
						<input type="radio" name="sel_oneweekonoff" value="oneweekonoff">Select One Week on One Week Off<br>
					</div>
					
					<div>	
						<label for="exchange_occur_day">Exchange to occur each</label>
						<input type="text" name="exchange_occur_day" placeholder="Day" id="exchange_occur_day"> at <input type="text" name="exchange_occur_time" placeholder="Time" id="exchange_occur_time">
					</div>
					<label for="exchange_occur_day">Transportation shall be provided by the exchanging parent.</label>
					<input type="radio" id="transportation" name="transportation" value="yes"> Yes<br>
				<input type="radio" id="transportation" name="transportation" value="no">No<br>
				<h2>Deviations</h2>
					<h3>You may request an amount of child support that us lower or higher than the amount in 3 or 4 but you reason(s) must be based upon one of the following factor.(Check all that apply)</h3>
					
					<div class="checkboxes_left checkboxes_list" >
					<input type="checkbox" name="cost_health_ins" value="cost of health insurance">The Cost of health insurance<br>
					<input type="checkbox" name="cost_childcare" value="cost of childcare">The Cost of childcare
					<br>
					<input type="checkbox" name="cost_specialneeds" value="special education needs">Special education needs<br>
					<input type="checkbox" name="age_child" value="age of child">Age of child
					<br>
					<input type="checkbox" name="parents_leg_res" value="parents legal responsibility">Parent's legal responsibility
					<br>
					<input type="checkbox" name="val_of_services_conteither" value="value of services contributed by either parent">Value of services contributed by either parent
					<br>
					<input type="checkbox" name="pub_ass_paid_supp" value="public assistance paid to support child">Public assistance paid to support the child
					<br>
					</div>
					
					<div class="checkboxes_left checkboxes_list" >
					<input type="checkbox" name="exp_reas_rel_mother" value="expense reasonably related to the mother's pregnancy and confinement">Expense reasonably related to the mother's pregnancy and confinement
					<br>
					<input type="checkbox" name="cost_trans_visitation" value="cost of transportation for visitation if custodian parent moves">Cost of transportation for visitation if custodian parent moved out of the jurisdiction
					<br>
					<input type="checkbox" name="am_time_child_spend_parent" value="am time ch spnd parent">The amount of time the child spends with each parent
					<br>
					<input type="checkbox" name="other_necess_exp_child" value="other necc exp child">Any other necessary expense for the benefit of child
					<br>					
					<input type="checkbox" name="rel_inc_bparent" value="Rel inc parent">The relative income of both parents
					<br>
					</div>
					
					<h3>Explain: <u>                                                    
					</u></h3>
					<div class="gform_footer top_label"> 
   <input type="submit" name="submit_info" id="submit_info" class="gform_button button" value="Submit"> 
   </div>
</form>
   ====================
</div>
<script>
jQuery(document).ready(function () {                            
    jQuery("#wife_resident_nev").change(function () {
        if (jQuery("#wife_resident_nev").is(":checked")) {
            jQuery('#show_wife_resident_nev').show();
			console.log('if check chnages');
        }else{
			 jQuery('#show_wife_resident_nev').hide();
			console.log('else chnages');
		}
    });
  jQuery("#husband_resident_nev").change(function () {
        if (jQuery("#husband_resident_nev").is(":checked")) {
            jQuery('#show_husband_resident_nev').show();
			console.log('if check chnages');
        }else{
			 jQuery('#show_husband_resident_nev').hide();
			console.log('else chnages');
		}
    });
jQuery('input[name=wife_emp]').change(function () {
		 if (jQuery("#wife_emp_yes").is(":checked")) {
            jQuery('#emp_wife_fields').show();
        }
        else if (jQuery("#wife_emp_no").is(":checked")) {
            jQuery('#emp_wife_fields').hide();
        }
    });
	
	jQuery('input[name=husband_emp]').change(function () {
		 if (jQuery("#husband_emp_yes").is(":checked")) {
            jQuery('#emp_husband_feilds').show();
        }
        else if (jQuery("#husband_emp_no").is(":checked")) {
            jQuery('#emp_husband_feilds').hide();
        }
    });
	jQuery('input[name=were_huswife_married]').change(function () {
		 if (jQuery("#in_usa").is(":checked")) {
            jQuery('#in_side_usa').show();
			jQuery('#out_side_usa').hide();
        }
        else if (jQuery("#out_usa").is(":checked")) {
            jQuery('#out_side_usa').show();
			jQuery('#in_side_usa').hide();
        }
    });
	jQuery('input[name=spouse_curr]').change(function () {
		 if (jQuery("#spouse_curr_yes_check").is(":checked")) {
            jQuery('#spouse_curr_yes').show();
			jQuery('#spouse_curr_no').hide();
        }
        else if (jQuery("#spouse_curr_no_check").is(":checked")) {
            jQuery('#spouse_curr_no').show();
			jQuery('#spouse_curr_yes').hide();
        }
    });
	jQuery('input[name=marital_home]').change(function () {
		 if (jQuery("#marital_home_yes").is(":checked")) {
            jQuery('#show_marital_home_adress').show();
        }
        else if (jQuery("#marital_home_no").is(":checked")) {
			jQuery('#show_marital_home_adress').hide();
        }
    });
	jQuery('input[name=marital_home_action]').change(function () {
		 if (jQuery("#marital_home_transferred").is(":checked")) {
            jQuery('#home_transferrer').show();
            jQuery('#home_sold').hide();
        }
        else if (jQuery("#marital_home_sold").is(":checked")) {
			jQuery('#home_transferrer').hide();
			jQuery('#home_sold').show();
        }
    });
	jQuery('input[name=wife_pregnant]').change(function (){
		 if (jQuery("#marital_home_yes").is(":checked")) {
            jQuery('#is_pregnant').show();
        }
        else if (jQuery("#marital_home_no").is(":checked")) {
			jQuery('#is_pregnant').hide();
        }
    });
});

</script>
<?php
$result = ob_get_clean();
return $result;
}
//============================================================+
// END OF FILE
//============================================================+
