<?php
/*
Plugin Name:  Plz to Page
Description:  Plz to Page plugin for getting zip radius of the nearest services.
Plugin URI: https://www.fiverr.com/wpright
Author:       WpRight
Version:      1.0.0
Text Domain:  plz-to-page
Domain Path:  /languages
License:      GPL v2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.txt
*/

if ( !class_exists( 'WP_Store_locator' ) ) {

	class WP_Store_locator {
        
        /**
         * Class constructor
         */          
        function __construct() {
                                    
            $this->define_constants();
            $this->includes();
            $this->plugin_settings();
            
            $this->post_types = new WPSL_Post_Types();
            $this->i18n       = new WPSL_i18n();
            $this->templates  = new WPSL_Templates();
                        
            register_activation_hook( __FILE__, array( $this, 'install' ) );
        }
        
        /**
         * Setup plugin constants.
         *
         * @since 1.0.0
         * @return void
         */
        public function define_constants() {

            if ( !defined( 'WPSL_VERSION_NUM' ) )
                define( 'WPSL_VERSION_NUM', '2.2.11' );

            if ( !defined( 'WPSL_URL' ) )
                define( 'WPSL_URL', plugin_dir_url( __FILE__ ) );

            if ( !defined( 'WPSL_BASENAME' ) )
                define( 'WPSL_BASENAME', plugin_basename( __FILE__ ) );

            if ( !defined( 'WPSL_PLUGIN_DIR' ) )
                define( 'WPSL_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
        }
        
        /**
         * Include the required files.
         *
         * @since 2.0.0
         * @return void
         */
        public function includes() {

            require_once( WPSL_PLUGIN_DIR . 'inc/wpsl-functions.php' );
            require_once( WPSL_PLUGIN_DIR . 'inc/class-templates.php' );
            require_once( WPSL_PLUGIN_DIR . 'inc/class-post-types.php' );
            require_once( WPSL_PLUGIN_DIR . 'inc/class-i18n.php' );
            require_once( WPSL_PLUGIN_DIR . 'frontend/class-frontend.php' );
            
            if ( is_admin() || defined( 'WP_CLI' ) && WP_CLI ) {
                require_once( WPSL_PLUGIN_DIR . 'admin/roles.php' );
                require_once( WPSL_PLUGIN_DIR . 'admin/class-admin.php' );
            }
        }
        
        /**
         * Setup the plugin settings.
         *
         * @since 2.0.0
         * @return void
         */
        public function plugin_settings() {
            
            global $wpsl_settings, $wpsl_default_settings;
            
            $wpsl_settings         = wpsl_get_settings();
            $wpsl_default_settings = wpsl_get_default_settings();
        }
        
        /**
         * Install the plugin data.
         *
         * @since 2.0.0
         * @return void
         */
        public function install( $network_wide ) {
            require_once( WPSL_PLUGIN_DIR . 'inc/install.php' );
            wpsl_install( $network_wide );
        }
	}
	
	$GLOBALS['wpsl'] = new WP_Store_locator();
}